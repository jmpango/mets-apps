package org.mets.mobile.async;

import org.mets.mobile.model.METSGlobalVariables;
import org.mets.mobile.service.ws.METSWebService;
import org.mets.mobile.util.Helper;
import org.mets.mobile.util.ConnectionDetector;
import org.mets.mobile.util.dialog.LoadingUI;
import android.app.FragmentManager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

/**
 * Used to fetch default startup data from the server.
 * 
 * @author jompango@gmail.com
 *
 */
@SuppressLint("ShowToast")
public class DefaultDataAsync extends AsyncTask<String, Void, String> {
	private METSWebService webService;
	private METSGlobalVariables globalVariable;
	private ConnectionDetector cDetector;
	private Context mContext;
	private FragmentManager fragmentManager;

	public DefaultDataAsync(Context mContext, FragmentManager fragmentManager) {
		this.globalVariable = (METSGlobalVariables) mContext.getApplicationContext();
		this.webService = globalVariable.getwService();
		this.mContext = mContext;
		this.fragmentManager = fragmentManager;
	}

	@SuppressLint("NewApi")
	@Override
	protected void onPreExecute() {
		cDetector = new ConnectionDetector(mContext);
		globalVariable.setLoadingUI(new LoadingUI());
		globalVariable.getLoadingUI().show(fragmentManager, Helper.LOADING_DATA);
	}

	@Override
	protected String doInBackground(String... params) {
		if (cDetector.isConnectingToInternet()) {
			webService.getBPlusKPI(globalVariable);
			webService.getPedKPI(globalVariable);
			webService.getRetKPI(globalVariable);

			/*webService.getWeeks(globalVariable, Helper.DEFAULT_LIMIT_10);
			webService.getMonts(globalVariable, Helper.DEFAULT_LIMIT_10, Helper.PED_KEY);
			webService.getMonts(globalVariable, Helper.DEFAULT_LIMIT_10, Helper.RET_KEY);*/
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		if (!cDetector.isConnectingToInternet()) {
			Toast.makeText(mContext, Helper.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG);
		}
		globalVariable.getLoadingUI().customDissmis();
		globalVariable.kpiFlag = true;
		globalVariable.pedFlag = true;
		globalVariable.retFlag = true;
	}

}
