package org.mets.mobile.async;

import org.mets.mobile.model.METSGlobalVariables;
import org.mets.mobile.service.ws.METSWebService;
import org.mets.mobile.util.ConnectionDetector;
import org.mets.mobile.util.Helper;
import org.mets.mobile.util.dialog.LoadingUI;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentManager;
import android.os.AsyncTask;
import android.widget.Toast;

public class OptionBPlusAsync extends AsyncTask<String, Void, String>{
	private METSWebService webService;
	private METSGlobalVariables globalVariable;
	private ConnectionDetector cDetector;
	private FragmentManager fragmentManager;
	private Activity mActivity;

	public OptionBPlusAsync(Activity mActivity, FragmentManager fragmentManager) {
		this.globalVariable = (METSGlobalVariables) mActivity.getApplication();
		this.webService = globalVariable.getwService();
		this.mActivity = mActivity;
		this.fragmentManager = fragmentManager;
	}

	@SuppressLint("NewApi")
	@Override
	protected void onPreExecute() {
		cDetector = new ConnectionDetector(mActivity.getApplicationContext());
		globalVariable.setLoadingUI(new LoadingUI());
		globalVariable.getLoadingUI().show(fragmentManager, Helper.LOADING_DATA);
	}

	@Override
	protected String doInBackground(String... params) {
		if (cDetector.isConnectingToInternet()) {
			webService.getBPlusKPI(globalVariable);
		}
		return null;
	}

	@SuppressLint("ShowToast")
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		if (!cDetector.isConnectingToInternet()) {
			Toast.makeText(mActivity, Helper.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG);
		}
		
		globalVariable.getApiListner().newServerData(globalVariable.getmService().getBPlusKPI(mActivity,
				globalVariable.getKpiEntity() + globalVariable.getKpiWeek()));
		globalVariable.getLoadingUI().customDissmis();
		
	}
}
