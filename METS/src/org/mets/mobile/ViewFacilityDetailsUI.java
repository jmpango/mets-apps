package org.mets.mobile;

import java.util.ArrayList;
import java.util.List;

import org.mets.mobile.adapter.SectionListViewAdapter;
import org.mets.mobile.model.BPlusReport;
import org.mets.mobile.model.EntryItem;
import org.mets.mobile.model.Facility;
import org.mets.mobile.model.FacilityContacts;
import org.mets.mobile.model.Item;
import org.mets.mobile.model.METSGlobalVariables;
import org.mets.mobile.util.dialog.CallUI;
import org.mets.mobile.util.dialog.WeeklyReportUI;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This is the startup for showing the Details of a facility
 * 
 * @author jmpango@gmail.com
 *
 */
public class ViewFacilityDetailsUI extends Activity {

	public static final String TAG = "View Facility Details";
	private Facility facility;
	private String parentTitle;
	private METSGlobalVariables globalVariables;
	private List<FacilityContacts> contacts = new ArrayList<>();
	private List<BPlusReport> reports = new ArrayList<>();

	public ViewFacilityDetailsUI() {
	}

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_facility_detail);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		this.globalVariables = (METSGlobalVariables) this.getApplication();

		TextView parentPageTitleTxtField = (TextView) findViewById(R.id.parent_page_title_txt);
		TextView pageTitleTxtField = (TextView) findViewById(R.id.page_title_txt);
		TextView callTxtField = (TextView) findViewById(R.id.txt_call);
		TextView viewReportTxtField = (TextView) findViewById(R.id.txt_view_report);
		ListView listView = (ListView) findViewById(R.id.listView);
		ImageView lastNextImg = (ImageView) findViewById(R.id.last_next_img);

		lastNextImg.setVisibility(View.INVISIBLE);

		viewReportTxtField.setOnClickListener(new CallClickListner(1, this));

		callTxtField.setOnClickListener(new CallClickListner(0, this));

		Intent intent = getIntent();

		this.facility = (Facility) intent.getSerializableExtra("facility");
		this.parentTitle = intent.getStringExtra("parentTitle");

		callTxtField.setText(Html.fromHtml("<u>Call</u>"));
		viewReportTxtField.setText(Html.fromHtml("<u>Weekly Reports</u>"));

		ArrayList<Item> facilityDetails = new ArrayList<Item>();

		contacts = globalVariables.getmService().getUserContacts(this, facility.getId() + "");
		reports = globalVariables.getmService().getBPlusReports(this, facility.getId() + "");

		facilityDetails.add(new EntryItem("Name", facility.getName()));
		facilityDetails.add(new EntryItem("Contact Persons", (contacts == null) ? "" : contacts.size() + ""));
		facilityDetails.add(new EntryItem("Supporting IP", facility.getSupportingIP()));
		facilityDetails.add(new EntryItem("Region", facility.getRegion()));
		facilityDetails.add(new EntryItem("District", facility.getDistrict()));
		facilityDetails.add(new EntryItem("Level", facility.getName()));
		facilityDetails.add(new EntryItem("ART Accredited", facility.getIsAcredited()));

		listView.setAdapter(new SectionListViewAdapter(this, facilityDetails, 3));

		parentPageTitleTxtField.setText(parentTitle);
		pageTitleTxtField.setText(facility.getName());

		new LoadFacilityDetails(this).execute();

	}

	private class LoadFacilityDetails extends AsyncTask<String, Void, String> {
		boolean isContactAvailable = false, isReportsAvailable;
		private Activity mActivity;
		
		public LoadFacilityDetails(Activity mActiviy) {
			this.mActivity = mActiviy;
		}

		@Override
		protected String doInBackground(String... params) {

			if (contacts == null) {
				isContactAvailable = globalVariables.getwService().getUserContacts(globalVariables,
						facility.getId() + "");
			}

			if (reports == null)
				isReportsAvailable = globalVariables.getwService().getBPlusReports(globalVariables, facility.getId(), 5);

			return null;
		}

		@SuppressLint("ShowToast")
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			if (isContactAvailable){
				contacts = globalVariables.getmService().getUserContacts(mActivity, facility.getId() + "");
				Toast.makeText(mActivity, "Facility contacts saved",
						Toast.LENGTH_LONG);
				
			}

			if (isReportsAvailable){
				reports = globalVariables.getmService().getBPlusReports(mActivity, "R:"+facility.getId() + "");
				Toast.makeText(mActivity, "Facility B+ reports saved",
						Toast.LENGTH_LONG);
			}

			if (isContactAvailable == false || isReportsAvailable == false)
				new LoadFacilityDetails(mActivity).execute();

		}

	}

	public class CallClickListner implements OnClickListener {
		int clickType = 0;
		Context mContext;

		public CallClickListner(int clickType, Context mContext) {
			this.clickType = clickType;
			this.mContext = mContext;
		}

		@SuppressLint({ "NewApi", "ShowToast" })
		@Override
		public void onClick(View v) {
			if (clickType == 0) {

				if (contacts != null) {
					if (contacts.size() > 0) {
						List<String> concatNumbers = new ArrayList<String>();
						for (FacilityContacts c : contacts) {
							concatNumbers.add(c.getFirstname() + " " + c.getUsername() + ": " + c.getPhone_number());
						}
						CallUI callDialog = new CallUI(concatNumbers);
						callDialog.show(getFragmentManager(), "Call Dialog");
					}else {
						Toast.makeText(mContext, "No contacts to show for this facilituy",
								Toast.LENGTH_LONG);
					}

				} else {
					Toast.makeText(mContext, "Wait, loading contacts from the server",
							Toast.LENGTH_LONG);
				}

			} else {
				if (reports != null) {
					if (reports.size() > 0) {
						WeeklyReportUI reportDialog = new WeeklyReportUI(facility, reports);
						reportDialog.show(getFragmentManager(), "Weekly Report Dialog");
					}else {
						Toast.makeText(mContext, "No reports to show for this facility",
								Toast.LENGTH_LONG);
					}
				} else {
					Toast.makeText(mContext, "Wait, still loading reports from the server",
							Toast.LENGTH_LONG);
				}
			}
		}

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		this.finish();
	}

}
