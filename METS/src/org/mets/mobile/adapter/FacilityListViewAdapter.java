package org.mets.mobile.adapter;

import java.util.Collections;
import java.util.List;

import org.mets.mobile.R;
import org.mets.mobile.model.Facility;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * This class is responsible for populating a list with facilities
 * @author Mpango
 *
 */
public class FacilityListViewAdapter extends ArrayAdapter<Facility> {
	public Context mContext;
	private List<Facility> facilities;
	private LayoutInflater vi;	

	public FacilityListViewAdapter(Context context,  List<Facility> facilities) {
		super(context, 0, facilities);
		this.mContext = context;
		this.facilities = facilities;
		Collections.sort(facilities);
		vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;

		final Facility facility = facilities.get(position);
		if (facility != null) {
			Facility mFacility = (Facility) facility;
			v = vi.inflate(R.layout.listview_row_two, null);
			final TextView lableName = (TextView) v.findViewById(R.id.nameLabel);
			final TextView levelHirrachy = (TextView) v.findViewById(R.id.value_label);

			if (lableName != null)
				lableName.setText(mFacility.getName());
			if (levelHirrachy != null)
				levelHirrachy.setText(
						mFacility.getRegion() + " / " + mFacility.getSubCounty() + " / " + mFacility.getDistrict());
		}

		return v;
	}

}
