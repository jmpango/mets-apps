package org.mets.mobile.adapter;

import java.util.ArrayList;

import org.mets.mobile.R;
import org.mets.mobile.model.EntryItem;
import org.mets.mobile.model.Item;
import org.mets.mobile.model.SectionItem;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Adapter to populate the listview with data
 * 
 * @author jmpango@gmail.com
 *
 */
public class SectionListViewAdapter extends ArrayAdapter<Item> {
	public Context mContext;
	private ArrayList<Item> items;
	private LayoutInflater vi;
	private int viewType = 0;

	public SectionListViewAdapter(Context context, ArrayList<Item> items, int viewType) {
		super(context, 0, items);
		this.mContext = context;
		this.items = items;
		this.viewType = viewType;
		vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;

		final Item i = items.get(position);
		if (i != null) {
			if (i.isSection()) {
				SectionItem si = (SectionItem) i;
				v = vi.inflate(R.layout.section_listview_header, null);

				v.setOnClickListener(null);
				v.setOnLongClickListener(null);
				v.setLongClickable(false);

				final TextView sectionView = (TextView) v.findViewById(R.id.section_title);
				sectionView.setText(si.getTitle());
			} else {
				EntryItem ei = (EntryItem) i;
				if (viewType == 0) {
					v = vi.inflate(R.layout.listview_row_one, null);
				} else if (viewType == 2) {
					v = vi.inflate(R.layout.listview_row_two, null);
				} else if (viewType == 4) {
					v = vi.inflate(R.layout.listview_row_four, null);
				} else
					v = vi.inflate(R.layout.listview_row_three, null);

				final TextView lableName = (TextView) v.findViewById(R.id.nameLabel);
				final TextView value = (TextView) v.findViewById(R.id.value_label);
				final ImageView navIcon = (ImageView) v.findViewById(R.id.nav_icon);

				if (lableName != null)
					lableName.setText(ei.getLableName());
				if (value != null)
					value.setText(ei.getValue());

				if (ei.getLableName().equalsIgnoreCase("Reporting rate")) {
					navIcon.setVisibility(View.INVISIBLE);
				}
			}
		}
		return v;
	}

}
