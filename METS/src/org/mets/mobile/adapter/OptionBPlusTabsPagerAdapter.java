package org.mets.mobile.adapter;

import org.mets.mobile.AncUI;
import org.mets.mobile.EidUI;
import org.mets.mobile.ReportingRatesUI;
import org.mets.mobile.StockOutUI;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class OptionBPlusTabsPagerAdapter extends FragmentPagerAdapter {
	private android.app.FragmentManager parentFragmentManager;

	public OptionBPlusTabsPagerAdapter(FragmentManager fm, android.app.FragmentManager parentFragmentManager) {
		super(fm);
		this.parentFragmentManager = parentFragmentManager;
	}

	@Override
	public Fragment getItem(int index) {

		switch (index) {
		case 0:
			return new ReportingRatesUI(parentFragmentManager);
		case 1:
			return new StockOutUI(parentFragmentManager);
		case 2:
			return new AncUI(parentFragmentManager);
		case 3:
			return new EidUI(parentFragmentManager);
		}

		return null;
	}

	@Override
	public int getCount() {
		return 4;
	}

}