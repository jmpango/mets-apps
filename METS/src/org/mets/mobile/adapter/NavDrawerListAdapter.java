package org.mets.mobile.adapter;

import java.util.ArrayList;

import org.mets.mobile.R;
import org.mets.mobile.model.NavigationDrawerItem;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * This class is responsible for binding the navigation items on the drawer
 * 
 * @author jompango@gmail.com
 * 
 */
public class NavDrawerListAdapter extends BaseAdapter {

	private Context mContext;
	private ArrayList<NavigationDrawerItem> navDrawerItems;

	public NavDrawerListAdapter(Context mContext,
			ArrayList<NavigationDrawerItem> navDrawerItems) {
		this.mContext = mContext;
		this.navDrawerItems = navDrawerItems;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) mContext
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(
					R.layout.nav_item_row, null);
		}

		ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
		TextView txtCount = (TextView) convertView.findViewById(R.id.counter);

		imgIcon.setImageResource(navDrawerItems.get(position).getIcon());
		txtTitle.setText(navDrawerItems.get(position).getTitle());

		if (navDrawerItems.get(position).getCounterVisibility()) {
			txtCount.setText(navDrawerItems.get(position).getCount());
		} else {
			txtCount.setVisibility(View.GONE);
		}

		return convertView;
	}
}
