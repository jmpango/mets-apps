package org.mets.mobile;

import java.util.ArrayList;

import org.mets.mobile.adapter.SectionListViewAdapter;
import org.mets.mobile.model.BPlusKPI;
import org.mets.mobile.model.EntryItem;
import org.mets.mobile.model.Item;
import org.mets.mobile.model.METSGlobalVariables;
import org.mets.mobile.model.SectionItem;
import org.mets.mobile.ui.listners.FacilityListItemClick;
import org.mets.mobile.ui.listners.ServerListener;
import org.mets.mobile.util.Helper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

/**
 * UI for Option B ANC
 * 
 * @author jompango@gmail.com
 *
 */
public class AncUI extends Fragment implements ServerListener {

	private ListView ancListView;
	private TextView ancRefreshTextView;
	private TextView ancDataTitle;

	private FragmentManager ancParentFragmentManager;
	private Activity ancActivity;

	private METSGlobalVariables ancGlobalVariables;
	private BPlusKPI ancKPI;

	public AncUI(FragmentManager parentFragmentManager) {
		this.ancParentFragmentManager = parentFragmentManager;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		View rootView = inflater.inflate(R.layout.optionb_ui, container, false);

		this.ancActivity = this.getActivity();
		this.ancGlobalVariables = (METSGlobalVariables) ancActivity.getApplication();

		ancListView = (ListView) rootView.findViewById(R.id.listView);
		TextView parentPageTitle = (TextView) rootView.findViewById(R.id.parent_page_title_txt);
		TextView pageTitle = (TextView) rootView.findViewById(R.id.page_title_txt);
		ancDataTitle = (TextView) rootView.findViewById(R.id.title_txt);
		ancRefreshTextView = (TextView) rootView.findViewById(R.id.txt_last_updated);

		parentPageTitle.setText("Option B+");
		pageTitle.setText("ANC");

		ancKPI = ancGlobalVariables.getmService().getBPlusKPI(ancActivity,
				ancGlobalVariables.getKpiEntity() + ancGlobalVariables.getKpiWeek());

		if (ancKPI != null)
			addItemsOnListView(ancKPI);
		else
			new populateUIAsync().execute();

		return rootView;
	}

	private void addItemsOnListView(BPlusKPI optionBKPI) {
		ancDataTitle.setText(ancGlobalVariables.getKpiEntity() + " " + ancGlobalVariables.getKpiWeek());
		ancRefreshTextView.setText(
				"Last updated: " + ancGlobalVariables.getmService().getLastRefreshed(ancActivity, Helper.KPI_KEY));

		ArrayList<Item> items = new ArrayList<Item>();
		items.add(new SectionItem("ANC1 Visit"));
		items.add(new EntryItem("# ANC1 Visit", optionBKPI.getAnc1Visits() + ""));
		items.add(new EntryItem("# Tested for HIV", optionBKPI.getTestedHIV() + ""));
		items.add(new EntryItem("Proportion tested for HIV", optionBKPI.getPctTotalTested() + "%"));

		items.add(new SectionItem("HIV Positive Women"));
		items.add(new EntryItem("# Women tested positive", optionBKPI.getTestedPositive() + ""));
		items.add(new EntryItem("# Initiated on ART", optionBKPI.getInitiated() + ""));
		items.add(new EntryItem("Proportion initiated on ART", optionBKPI.getPctTotalIntiated() + "%"));

		ancListView.setAdapter(new SectionListViewAdapter(ancActivity, items, 3));
		ancListView.setOnItemClickListener(new FacilityListItemClick(ancActivity, items, 3));

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.optionbplus, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@SuppressLint("NewApi")
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return Helper.MenuAction(item, ancActivity, ancParentFragmentManager);
	}

	private class populateUIAsync extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			do {

			} while (!ancGlobalVariables.kpiFlag);

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			ancGlobalVariables.kpiFlag = false;
			ancKPI = ancGlobalVariables.getmService().getBPlusKPI(ancActivity,
					ancGlobalVariables.getKpiEntity() + ancGlobalVariables.getKpiWeek());
			addItemsOnListView(ancKPI);

		}

	}

	@Override
	public void newServerData(BPlusKPI kpi) {
		addItemsOnListView(kpi);
	}
}
