package org.mets.mobile;

import java.util.ArrayList;

import org.mets.mobile.adapter.SectionListViewAdapter;
import org.mets.mobile.model.BPlusKPI;
import org.mets.mobile.model.EntryItem;
import org.mets.mobile.model.Item;
import org.mets.mobile.model.METSGlobalVariables;
import org.mets.mobile.model.SectionItem;
import org.mets.mobile.ui.listners.FacilityListItemClick;
import org.mets.mobile.ui.listners.ServerListener;
import org.mets.mobile.util.Helper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

/**
 * UI for Option B reporting rates
 * 
 * @author jompango@gmail.com
 *
 */
public class ReportingRatesUI extends Fragment implements ServerListener {

	private TextView rRateRefreshTextView;
	private TextView rRateDataTitle;
	private ListView rRateListView;

	private FragmentManager rRateParentFragmentManager;
	private Activity rRateActivity;

	protected METSGlobalVariables rRateGlobalVariable;
	private BPlusKPI rRateKPI;

	public ReportingRatesUI(FragmentManager parentFragmentManager) {
		this.rRateParentFragmentManager = parentFragmentManager;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		View rootView = inflater.inflate(R.layout.optionb_ui, container, false);

		this.rRateActivity = this.getActivity();
		this.rRateGlobalVariable = (METSGlobalVariables) rRateActivity.getApplication();
		rRateGlobalVariable.setApiListner(this);

		rRateListView = (ListView) rootView.findViewById(R.id.listView);
		TextView parentPageTitle = (TextView) rootView.findViewById(R.id.parent_page_title_txt);
		TextView pageTitle = (TextView) rootView.findViewById(R.id.page_title_txt);
		rRateDataTitle = (TextView) rootView.findViewById(R.id.title_txt);
		rRateRefreshTextView = (TextView) rootView.findViewById(R.id.txt_last_updated);

		parentPageTitle.setText("Option B+");
		pageTitle.setText("R/Rates");

		rRateKPI = rRateGlobalVariable.getmService().getBPlusKPI(rRateActivity,
				rRateGlobalVariable.getKpiEntity() + rRateGlobalVariable.getKpiWeek());

		if (rRateKPI != null)
			addItemsOnListView(rRateKPI);
		else
			new populateUIAsync().execute();

		return rootView;
	}

	public void addItemsOnListView(BPlusKPI kpi) {
		rRateRefreshTextView.setText(
				"Last updated: " + rRateGlobalVariable.getmService().getLastRefreshed(rRateActivity, Helper.KPI_KEY));
		rRateDataTitle.setText(rRateGlobalVariable.getKpiEntity() + " " + rRateGlobalVariable.getKpiWeek());
		
		ArrayList<Item> items = new ArrayList<Item>();
		if(kpi != null){
			items.add(new SectionItem("Option B+ Weekly SMS Reporting"));
			items.add(new EntryItem("View registered facilities", kpi.getFacilitiesRegistered() + ""));
			items.add(new EntryItem("View facilities that reported", kpi.getReportsRecieved() + ""));
			items.add(new EntryItem("View facilities that did not report",
					(kpi.getFacilitiesRegistered() - kpi.getReportsRecieved()) + ""));
			items.add(new EntryItem("Reporting rate", kpi.getReportingRate() + "%"));

		}
		rRateListView.setAdapter(new SectionListViewAdapter(rRateActivity, items, 0));
		rRateListView.setOnItemClickListener(new FacilityListItemClick(rRateActivity, items, 0));

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.optionbplus, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@SuppressLint("NewApi")
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return Helper.MenuAction(item, rRateActivity, rRateParentFragmentManager);
	}

	private class populateUIAsync extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			do {

			} while (!rRateGlobalVariable.kpiFlag);

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			rRateGlobalVariable.kpiFlag = false;
			rRateKPI = rRateGlobalVariable.getmService().getBPlusKPI(rRateActivity,
					rRateGlobalVariable.getKpiEntity() + rRateGlobalVariable.getKpiWeek());
			addItemsOnListView(rRateKPI);
		}

	}

	@Override
	public void newServerData(BPlusKPI kpi) {
		addItemsOnListView(kpi);
	}

}
