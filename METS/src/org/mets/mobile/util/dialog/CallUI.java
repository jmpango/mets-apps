package org.mets.mobile.util.dialog;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;

/**
 * Used to display the contacts of of people to be called
 * 
 * @author jompango@gmail.com
 *
 */
@SuppressLint("NewApi")
public class CallUI extends DialogFragment {
	private static final String CALL = "Call";
	private static final String CANCEL = "Cancel";
	private List<String> contactList;
	private String selectedName;

	public CallUI(List<String> contactListz) {
		this.contactList = contactListz;
	}

	@Override
	public void onDestroyView() {
		if (getDialog() != null && getRetainInstance())
			getDialog().setDismissMessage(null);
		super.onDestroyView();
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);

		final CharSequence[] cs = contactList.toArray(new CharSequence[contactList.size()]);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(Html.fromHtml(
				"<b>Select number to call </b><br/><p style= \"color:red; font-size:160%; text-align:center\">Normal calling rates do apply</p>"));

		builder.setSingleChoiceItems(cs, -1, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				selectedName = cs[which] + "";
			}
		}).setPositiveButton(CALL, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				int start = selectedName.indexOf(":") + 1;
				int end = selectedName.length();

				if(!selectedName.substring(start, end).equalsIgnoreCase("")){
					String phoneNo = "tel:" + "+" + selectedName.substring(start, end);
					Intent callIntent = new Intent(Intent.ACTION_CALL);
					callIntent.setData(Uri.parse(phoneNo));
					startActivity(callIntent);
				}
				dialog.dismiss();
			}
		}).setNegativeButton(CANCEL, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		return builder.create();
	}
}
