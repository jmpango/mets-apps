package org.mets.mobile.util.dialog;

import java.io.Serializable;

import org.mets.mobile.R;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

/**
 * Used to show on screen as data is loaded from ASYNC processes
 * 
 * @author jompango@gmail.com
 *
 */
@SuppressLint("NewApi")
public class LoadingUI extends DialogFragment implements Serializable{
	private static final long serialVersionUID = 1L;

	public LoadingUI() {
	}

	@Override
	public void onDestroyView() {
		if (getDialog() != null && getRetainInstance())
			getDialog().setDismissMessage(null);
		super.onDestroyView();
	}

	@SuppressLint("InflateParams")
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		View customView = LayoutInflater.from(getActivity()).inflate(R.layout.processing_ui, null, false);
		builder.setView(customView);

		AlertDialog alert = builder.create();
		//alert.setCanceledOnTouchOutside(true);
		return alert;

	}

	public void customDissmis() {
		this.dismiss();
		//this.getDialog().cancel();
		//globalVariables.getContentLoadingLayout().setVisibility(LinearLayout.GONE);
	}

}
