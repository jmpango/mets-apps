package org.mets.mobile.util.dialog;

import java.util.ArrayList;
import java.util.List;

import org.mets.mobile.R;
import org.mets.mobile.adapter.SectionListViewAdapter;
import org.mets.mobile.model.BPlusReport;
import org.mets.mobile.model.EntryItem;
import org.mets.mobile.model.Facility;
import org.mets.mobile.model.Item;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

@SuppressLint("NewApi")
public class WeeklyReportUI extends DialogFragment {
	private static final String OK = "DONE";
	private Facility facility;
	private List<BPlusReport> reports;

	public WeeklyReportUI() {
	}

	public WeeklyReportUI(Facility facility, List<BPlusReport> reports) {
		this.facility = facility;
		this.reports = reports;
	}

	@Override
	public void onDestroyView() {
		if (getDialog() != null && getRetainInstance())
			getDialog().setDismissMessage(null);
		super.onDestroyView();
	}

	@SuppressLint("InflateParams")
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		View customView = LayoutInflater.from(getActivity()).inflate(R.layout.sub_weekly_report_ui, null, false);
		ListView listView = (ListView) customView.findViewById(R.id.listView);
		TextView titleTextView = (TextView) customView.findViewById(R.id.txt_weekly_report_title);
		TextView indicatorTextView = (TextView) customView.findViewById(R.id.txt_weekly_report_indicator);

		indicatorTextView.setText(Html.fromHtml(getString(R.string.defaut_weekly_report_indicators)));
		titleTextView.setText(Html.fromHtml("<b>Ministry of Health</b><br/>" + facility.getSupportingIP()
				+ "</b><br/>OptionB+ Weekly Reports<br/><br/>" + facility.getName()));

		ArrayList<Item> weeklyReport = new ArrayList<Item>();

		for (BPlusReport r : reports) {
			String testkit = r.getH();
			String arv = r.getI();

			if (testkit.equals("true"))
				testkit = "Y";
			else
				testkit = "N";

			if (arv.equals("true"))
				arv = "Y";
			else
				arv = "N";

			weeklyReport.add(new EntryItem(r.getWeekno(),
					"a = " + r.getA() + ", " + "b = " + r.getB() + ", " + "c = " + r.getC() + ", " + "d = " + r.getD()
							+ ", " + "e = " + r.getE() + ", " + "f = " + r.getF() + ", " + "g = " + r.getG() + ", "
							+ "h = " + testkit + ", " + "i = " + arv));
		}

		// weeklyReport.add(new EntryItem("2015W26", "a = 6, b = 0, c = 0, d =
		// 0, e = 0, f = 0, g = 1, h = N, i = Y"));
		listView.setAdapter(new SectionListViewAdapter(getActivity().getApplicationContext(), weeklyReport, 4));

		builder.setView(customView).setPositiveButton(OK, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
				dialog.cancel();
			}
		});
		return builder.create();
	}
}
