package org.mets.mobile.util.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Represents a YesNO custom dialog for METS App
 * 
 * @author jompango@gmail.com
 *
 */
public class YesNoUI extends DialogFragment {
	private static final String YES = "Yes";
	private static final String NO = "No";

	private String headerMessage;

	public YesNoUI(String headerMessage) {
		this.headerMessage = headerMessage;
	}

	@Override
	public void onDestroyView() {
		if (getDialog() != null && getRetainInstance())
			getDialog().setDismissMessage(null);
		super.onDestroyView();
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("SI Health Information");
		builder.setMessage(headerMessage).setPositiveButton(YES, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		}).setNegativeButton(NO, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});
		return builder.create();
	}
}
