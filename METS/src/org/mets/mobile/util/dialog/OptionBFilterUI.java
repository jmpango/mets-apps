package org.mets.mobile.util.dialog;

import java.util.ArrayList;
import java.util.List;

import org.mets.mobile.R;
import org.mets.mobile.async.OptionBPlusAsync;
import org.mets.mobile.model.BPlusFilter;
import org.mets.mobile.model.METSGlobalVariables;
import org.mets.mobile.util.Helper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class OptionBFilterUI extends DialogFragment {
	private static final String FILTER = "Filter";
	private static final String CANCEL = "Cancel";

	private Spinner spinnerWeek;
	private Spinner spinnerLevel;
	private Spinner spinnerIPorDistrict;

	private TextView loadingTextView;
	private ProgressBar loadingProgressBar;

	private List<String> levelCategories;

	private BPlusFilter iFilter;

	private METSGlobalVariables globalVariable;
	private FragmentManager parentFragmentManager;
	private Activity parentActivity;
	private Activity currentActivity;
	
	/*
	 * public OptionBFilterUI(Context parentContext, ) { }
	 */

	public OptionBFilterUI(Activity mActivity, FragmentManager parentFragmentManager) {
		this.parentActivity = mActivity;
		this.parentFragmentManager = parentFragmentManager;
	}

	@Override
	public void onDestroyView() {
		if (getDialog() != null && getRetainInstance())
			getDialog().setDismissMessage(null);
		super.onDestroyView();
	}

	@SuppressLint("InflateParams")
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);

		this.currentActivity = this.getActivity();
		this.globalVariable = (METSGlobalVariables) currentActivity.getApplication();

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		iFilter = new BPlusFilter();

		View customView = LayoutInflater.from(getActivity()).inflate(R.layout.optionb_filter_ui, null, false);
		TextView titleTextView = (TextView) customView.findViewById(R.id.txt_optionb_filter_title);
		loadingTextView = (TextView) customView.findViewById(R.id.loading);
		loadingProgressBar = (ProgressBar) customView.findViewById(R.id.filter_optionb_progress);

		spinnerWeek = (Spinner) customView.findViewById(R.id.filter_optionb_week);
		spinnerLevel = (Spinner) customView.findViewById(R.id.filter_optionb_level);
		spinnerIPorDistrict = (Spinner) customView.findViewById(R.id.filter_optionb_ip);

		spinnerWeek.setOnItemSelectedListener(new WeekSpinnerItemListner());
		spinnerLevel.setOnItemSelectedListener(new LevelSpinnerItemListner());
		spinnerIPorDistrict.setOnItemSelectedListener(new EntitySpinnerItemListner());

		hideProgressBar();
		
		populateSpinnerWeekDropDown(null);
		populateSpinnerLevelDropDown();
		
		new LoadWeekData().execute();

		titleTextView.setText(Html.fromHtml("<b><br/>Ministry of Health</b><br/>Option B+ Filter"));

		builder.setView(customView).setPositiveButton(CANCEL, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
				dialog.cancel();
			}
		}).setNegativeButton(FILTER, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {

				globalVariable.setKpiEntity(iFilter.getEntity());
				globalVariable.setKpiLevel(iFilter.getLevel());
				globalVariable.setKpiWeek(iFilter.getWeekNo());
				
				dialog.dismiss();
				dialog.cancel();

				new OptionBPlusAsync(currentActivity, parentFragmentManager).execute();

			}
		});

		return builder.create();
	}

	private class WeekSpinnerItemListner implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			Spinner spinner = (Spinner) parent;
			iFilter.setWeekNo(spinner.getItemAtPosition(position).toString());

		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {

		}

	}

	private class LevelSpinnerItemListner implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			Spinner spinner = (Spinner) parent;
			String selectedItem = spinner.getItemAtPosition(position).toString();
			iFilter.setLevel(selectedItem);
			populateSpinnerIPorDistrictDropDown(selectedItem);
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {

		}

	}

	private class EntitySpinnerItemListner implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			Spinner spinner = (Spinner) parent;
			iFilter.setEntity(spinner.getItemAtPosition(position).toString());
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {

		}

	}

	public void hideProgressBar() {
		loadingTextView.setVisibility(View.INVISIBLE);
		loadingProgressBar.setVisibility(View.INVISIBLE);
	}

	public void showProgressBar() {
		loadingTextView.setVisibility(View.VISIBLE);
		loadingProgressBar.setVisibility(View.VISIBLE);
	}

	private void populateSpinnerWeekDropDown(List<String> weeks) {
		if(weeks == null){
			weeks  = new ArrayList<String>();
			weeks.add(Helper.getCurrentReportingWeek());
		}else{
			spinnerWeek.setEnabled(true);
			spinnerLevel.setEnabled(true);
		}
		
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity().getBaseContext(),
				android.R.layout.simple_spinner_item, weeks);

		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerWeek.setAdapter(dataAdapter);
	}

	private void populateSpinnerLevelDropDown() {
		levelCategories = new ArrayList<String>();
		levelCategories.add("National");
		levelCategories.add("District");
		levelCategories.add("Implementing Partner");

		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity().getBaseContext(),
				android.R.layout.simple_spinner_item, levelCategories);

		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerLevel.setAdapter(dataAdapter);

	}

	private void populateSpinnerIPorDistrictDropDown(String selectedLevel) {
		List<String> entities = new ArrayList<String>();
		if (selectedLevel.equalsIgnoreCase("National")) {
			entities.add("Uganda");
			populateEntityDropdown(entities);
		} else if (selectedLevel.equals("District")) {
			entities = globalVariable.getmService().getEntities(currentActivity, "bplusDistrict");
			if (entities != null)
				populateEntityDropdown(entities);
			else {
				new LoadEntityData("District").execute();
			}

		} else {
			entities = globalVariable.getmService().getEntities(currentActivity, "bplusIP");
			if (entities != null)
				populateEntityDropdown(entities);
			else {
				new LoadEntityData("IP").execute();
			}
		}

	}

	private void populateEntityDropdown(List<String> entities) {
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity().getBaseContext(),
				android.R.layout.simple_spinner_item, entities);

		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerIPorDistrict.setAdapter(dataAdapter);
		spinnerIPorDistrict.setEnabled(true);
	}

	@SuppressLint("ShowToast")
	public class LoadWeekData extends AsyncTask<String, Void, String> {
		private boolean isWeekDataAvailable = false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			spinnerWeek.setEnabled(false);
			spinnerIPorDistrict.setEnabled(false);
			spinnerLevel.setEnabled(false);

			showProgressBar();
		}

		@Override
		protected String doInBackground(String... params) {
			List<String> weeks = globalVariable.getmService().getWeeks(currentActivity, Helper.WEEKS_KEY);
			
			if ( weeks == null)
				isWeekDataAvailable = globalVariable.getwService().getWeeks(globalVariable, 10);
			else{
				isWeekDataAvailable = true; 
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			hideProgressBar();
			if (isWeekDataAvailable)
				populateSpinnerWeekDropDown(globalVariable.getmService().getWeeks(currentActivity, Helper.WEEKS_KEY));
			else
				Toast.makeText(parentActivity, "Failed to retrieve weeks, cancel and try again.", Toast.LENGTH_LONG);
		}

	}

	@SuppressLint("ShowToast")
	public class LoadEntityData extends AsyncTask<String, Void, String> {
		private boolean isEntityAvailable = false;
		private String level;

		public LoadEntityData(String level) {
			this.level = level;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showProgressBar();
			spinnerIPorDistrict.setEnabled(false);
		}

		@Override
		protected String doInBackground(String... params) {
			isEntityAvailable = globalVariable.getwService().getEntity(globalVariable, "bplus", level);
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			hideProgressBar();
			spinnerIPorDistrict.setEnabled(true);
			if (isEntityAvailable)
				populateEntityDropdown(globalVariable.getmService().getEntities(currentActivity, "bplus" + level));
			else
				Toast.makeText(parentActivity, "Failed to retrieve entities, cancel and try again.", Toast.LENGTH_LONG);
		}

	}

}
