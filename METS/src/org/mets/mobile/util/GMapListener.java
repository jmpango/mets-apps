package org.mets.mobile.util;

public interface GMapListener {
	public boolean isInternetConnectionON();

	public boolean isGPSActive();
}
