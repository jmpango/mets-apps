package org.mets.mobile.util;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.mets.mobile.R;
import org.mets.mobile.model.Facility;
import org.mets.mobile.util.dialog.OptionBFilterUI;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentManager;
import android.view.MenuItem;
import android.widget.Toast;

/**
 * This has constant methods
 * 
 * @author jompango@gmail.com
 * 
 */
@SuppressLint("SimpleDateFormat")
public class Helper {

	public static final String DEFAULT_BPLUS_ENTITY = "Uganda";
	public static final String DEFAULT_BPLUS_LEVEL = "National";
	
	public static final String DEFAULT_RET_ENTITY = "Uganda";
	public static final String DEFAULT_RET_LEVEL = "National";
	
	public static final String DEFAULT_PED_ENTITY = "Uganda";
	public static final String DEFAULT_PED_LEVEL = "National";
	
	public static final String CACHE_DB = "Mets";
	public static final String KPI_KEY = "kpi";
	public static final String PED_KEY = "ped";
	public static final String RET_KEY = "ret";
	public static final String FACILITY_KEY = "facility";
	public static final int DEFAULT_LIMIT_10 = 10;
	public static final int DEFAULT_LIMIT_5 = 5;

	public static final String DEFAULT_CURRENT_DATE_KEY = new SimpleDateFormat("ddMMyyyy").format(new Date());
	public static final String LOADING_DATA = "Loading remote data";
	public static final CharSequence NO_INTERNET_CONNECTION = "No internet connection.";
	public static final String WEEKS_KEY = "weeks";

	public static String webServiceURL = "http://ws.mets.or.ug/webAPI/";

	public static String DEFAULT_PREVIOUS_DATE_KEY() {
		DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		Date myDate = new Date(System.currentTimeMillis());
		System.out.println("result is " + dateFormat.format(myDate));
		Calendar cal = Calendar.getInstance();
		cal.setTime(myDate);
		cal.add(Calendar.DATE, -1);
		return dateFormat.format(cal.getTime());
	}

	@SuppressLint("NewApi")
	public static boolean MenuAction(MenuItem item, Activity mActivity, FragmentManager fragmentManager) {
		switch (item.getItemId()) {
		case R.id.action_filter:
			new OptionBFilterUI(mActivity, fragmentManager).show(fragmentManager, "optiob filter Dialog");
			return true;
		/*case R.id.action_refresh:
			new LoadingUI().show(fragmentManager, "Loading Dialog");
			return true;*/
		default:
			return false;
		}
	}

	@SuppressLint("DefaultLocale")
	public static List<Facility> searchFacilityWithName(String searchText, List<Facility> facilities) {
		List<Facility> facilitiez = new ArrayList<Facility>();
		if(facilities != null){
			for (Facility facility : facilities) {
				if ((facility.getName().toLowerCase()).startsWith(searchText.toLowerCase())) {
					facilitiez.add(facility);
				}
			}
		}

		return facilitiez;
	}

	public static List<Facility> searchFacilityWithLevelType(String string, List<Facility> facilities) {
		List<Facility> facilitiez = new ArrayList<Facility>();
		for (Facility facility : facilities) {
			if (facility.getLevel().equalsIgnoreCase(string)) {
				facilitiez.add(facility);
			}
		}

		return facilitiez;
	}

	public static void showLMessage(String message, Activity activity) {
		Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
	}

	public static String getCurrentReportingWeek() {
		Calendar now = Calendar.getInstance();
		int reportingWeek = now.get(Calendar.WEEK_OF_YEAR) - 1;

		if (reportingWeek < 10 && reportingWeek != 0) {
			return now.get(Calendar.YEAR) + "W0" + reportingWeek;
		}

		if (reportingWeek == 0) {
			return now.get(Calendar.YEAR) - 1 + "W53";
		}

		return now.get(Calendar.YEAR) + "W" + reportingWeek;
	}

	public static String getPreviousReportingWeek() {
		Calendar now = Calendar.getInstance();
		int reportingWeek = now.get(Calendar.WEEK_OF_YEAR) - 2;

		if (reportingWeek < 10 && reportingWeek != 0) {
			return now.get(Calendar.YEAR) + "W0" + reportingWeek;
		}

		if (reportingWeek == 0) {
			return now.get(Calendar.YEAR) - 1 + "W53";
		}

		return now.get(Calendar.YEAR) + "W" + reportingWeek;
	}

	public static String getCurrentReportingMonth() {
		String month = "";
		Calendar now = Calendar.getInstance();
		int num = (now.get(Calendar.MONTH)) - 1;
		int year = now.get(Calendar.YEAR);
		
		if(num < 0){
			num = 11;
			year = year - 1;
		}

		DateFormatSymbols dfs = new DateFormatSymbols();
		String[] months = dfs.getMonths();
		if (num >= 0 && num <= 11) {
			month = months[num];
		}
		
		return month + " " + year;
	}

	/*
	 * public static boolean isNetworkAvailable(Context mContext) {
	 * 
	 * if (isNetworkAvailable(mContext)) { try { HttpURLConnection urlc =
	 * (HttpURLConnection) (new URL("http://www.google.com").openConnection());
	 * urlc.setRequestProperty("User-Agent", "Test");
	 * urlc.setRequestProperty("Connection", "close");
	 * urlc.setConnectTimeout(1500); urlc.connect(); return
	 * (urlc.getResponseCode() == 200); } catch (IOException e) { } } else {
	 * Log.d("METS Mobile", "No network available!"); } return false;
	 * 
	 * 
	 * ConnectivityManager connectivityManager = (ConnectivityManager) mContext
	 * .getSystemService(Context.CONNECTIVITY_SERVICE); NetworkInfo
	 * activeNetworkInfo = connectivityManager.getActiveNetworkInfo(); return
	 * activeNetworkInfo != null && activeNetworkInfo.isConnected();
	 * 
	 * // LocationManager locationManager = (LocationManager) //
	 * mContext.getSystemService(Context.LOCATION_SERVICE);
	 * 
	 * // return //
	 * locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER); }
	 */

}
