package org.mets.mobile.util;

import org.mets.mobile.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * This class is responsible to display an alertDialog
 * 
 * @author jompango@gmail.com
 * 
 */
public class AlertDialogManager {
	private boolean isDeleted = false;

	@SuppressWarnings("deprecation")
	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);

		if (status != null)
			alertDialog
					.setIcon((status) ? R.drawable.success : R.drawable.fail);

		// Setting OK Button
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		});

		// Showing Alert Message
		alertDialog.show();
	}

	public boolean showAlertDeleteDialog(final Context context, String title,
			String message) {

		final AlertDialog alertDialog = new AlertDialog.Builder(context)
				.setPositiveButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								isDeleted = false;
							}
						})
				.setNegativeButton("Delete",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								isDeleted = true;
							}
						}).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon(R.drawable.ic_delete);
		alertDialog.setCanceledOnTouchOutside(false);
		alertDialog.show();

		return isDeleted;
	}
}
