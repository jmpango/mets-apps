package org.mets.mobile.ui.listners;

import org.mets.mobile.model.BPlusKPI;

public interface ServerListener {
	
	public void newServerData(BPlusKPI kpi);

}
