package org.mets.mobile.ui.listners;

import org.mets.mobile.model.BPlusKPI;

/**
 * Responsible to check if there is any update for BPlus from the server.
 * @author jompango@gmail.com
 *
 */
public interface BPlusListner {
	public void monitorUpdate();
	public void addItemsOnListView(BPlusKPI kpi); 
}
