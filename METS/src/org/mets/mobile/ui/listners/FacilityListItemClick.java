package org.mets.mobile.ui.listners;

import java.util.ArrayList;

import org.mets.mobile.ViewFacilityUI;
import org.mets.mobile.model.EntryItem;
import org.mets.mobile.model.Item;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class FacilityListItemClick implements OnItemClickListener {

	private ArrayList<Item> items = new ArrayList<Item>();
	private Context mContext;
	private int levelType = 0;

	public FacilityListItemClick(Context context, ArrayList<Item> items, int levelType) {
		this.items = items;
		this.mContext = context;
		this.levelType = levelType;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if (!items.get(position).isSection() && levelType != 3) {

			EntryItem item = (EntryItem) items.get(position);

			if (!item.getLableName().equalsIgnoreCase("Reporting rate")) {
				if (!item.getValue().equalsIgnoreCase("0")) {
					
					String qFacilities = "";
					String parentTitle = "";
					
					switch (item.getLableName()) {
					case "View registered facilities":
						qFacilities = "Registered Facilities";
						parentTitle = "B+ Registered sites";
						break;
					case "View facilities that reported":
						qFacilities = "Facilities Reported";
						parentTitle = "B+ Reported";
						break;

					case "View facilities that did not report":
						qFacilities = "Facilities Not Reported";
						parentTitle = "B+ NOT Reported";
						break;

					case "View facilities without testkits":
						qFacilities = "Facilities Without Testkits";
						parentTitle = "B+ Without Testkits";
						break;

					case "View facilities without ARVs":
						qFacilities = "Facilities Without ARVs";
						parentTitle = "B+ Without ARVs";
						break;

					default:
						break;
					}

					Intent myIntent = new Intent(mContext, ViewFacilityUI.class);
					myIntent.putExtra("parentTitle", parentTitle);
					myIntent.putExtra("qFacilities", qFacilities);
					myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					mContext.startActivity(myIntent);

				} else {
					Toast.makeText(mContext, "No facilities to display", Toast.LENGTH_LONG).show();
				}
			}
		}

	}

}
