package org.mets.mobile;

import org.mets.mobile.async.DefaultDataAsync;
import org.mets.mobile.fragment.FacilityLocatorFragment;
import org.mets.mobile.fragment.OptionBPlusFragment;
import org.mets.mobile.fragment.PedFragment;
import org.mets.mobile.fragment.RetentionFragment;
import org.mets.mobile.fragment.nav.NavigationDrawerFragment;
import org.mets.mobile.fragment.nav.NavigationDrawerFragment.NavigationDrawerCallbacks;
import org.mets.mobile.model.METSGlobalVariables;
import org.mets.mobile.service.impl.METSServiceImpl;
import org.mets.mobile.service.ws.METSWebService;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

/**
 * Startup UI for the application
 * 
 * @author jompango@gmail.com
 * 
 */
@SuppressLint("NewApi")
public class MainUI extends ActionBarActivity implements NavigationDrawerCallbacks {

	public static final String TAG = MainUI.class.getSimpleName();

	private Fragment mVisible;
	private Fragment optionBPlusFragment;
	private Fragment retentionFragment;
	private Fragment pedFragment;
	private Fragment facilityLocatorFragment;
	private NavigationDrawerFragment mNavigationDrawerFragment;

	private METSGlobalVariables globalVariable;

	private CharSequence mTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_ui);
		globalVariable = (METSGlobalVariables) this.getApplication();

		globalVariable.setmService(new METSServiceImpl());
		globalVariable.setwService(new METSWebService());
		globalVariable.kpiFlag = false;
		globalVariable.pedFlag = false;
		globalVariable.retFlag = false;
		
		if (globalVariable.getDatabase().size() == 0) {
			new DefaultDataAsync(this, getFragmentManager()).execute();
		}

		mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager()
				.findFragmentById(R.id.navigation_drawer);

		mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
		mVisible = optionBPlusFragment;
		mTitle = getString(R.string.title_home);

		setUpUIFragments();
	}

	private void setUpUIFragments() {
		final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

		optionBPlusFragment = (Fragment) getSupportFragmentManager().findFragmentByTag(OptionBPlusFragment.TAG);

		if (optionBPlusFragment == null) {
			optionBPlusFragment = OptionBPlusFragment.newInstance(getFragmentManager());
			ft.add(R.id.content_frame, optionBPlusFragment, OptionBPlusFragment.TAG);
		}
		ft.show(optionBPlusFragment);

		retentionFragment = (Fragment) getSupportFragmentManager().findFragmentByTag(RetentionFragment.TAG);

		if (retentionFragment == null) {
			retentionFragment = RetentionFragment.newInstance("retention", getFragmentManager());
			ft.add(R.id.content_frame, retentionFragment, RetentionFragment.TAG);
		}
		ft.hide(retentionFragment);

		pedFragment = (Fragment) getSupportFragmentManager().findFragmentByTag(PedFragment.TAG);

		if (pedFragment == null) {
			pedFragment = PedFragment.newInstance("peadiatric");
			ft.add(R.id.content_frame, pedFragment, RetentionFragment.TAG);
		}
		ft.hide(pedFragment);

		facilityLocatorFragment = (FacilityLocatorFragment) getSupportFragmentManager()
				.findFragmentByTag(FacilityLocatorFragment.TAG);
		if (facilityLocatorFragment == null) {

			facilityLocatorFragment = FacilityLocatorFragment.newInstance("Coordinats");

			ft.add(R.id.content_frame, facilityLocatorFragment, FacilityLocatorFragment.TAG);
		}
		ft.hide(facilityLocatorFragment);

		ft.commit();
	}

	@SuppressLint("NewApi")
	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		switch (position) {
		case 0:
			showFragment(optionBPlusFragment);
			mTitle = getString(R.string.title_home);
			break;
		case 1:
			showFragment(retentionFragment);
			mTitle = getString(R.string.title_retention);
			break;
		case 2:
			showFragment(pedFragment);
			mTitle = getString(R.string.title_retention);
			break;
		default:
			showFragment(facilityLocatorFragment);
			mTitle = getString(R.string.title_facility_locator);
		}
	}

	private void showFragment(Fragment fragmentIn) {
		if (fragmentIn == null)
			return;

		final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

		if (mVisible != null)
			ft.hide(mVisible);

		ft.show(fragmentIn).commit();
		mVisible = fragmentIn;
	}

	public void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mNavigationDrawerFragment.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}