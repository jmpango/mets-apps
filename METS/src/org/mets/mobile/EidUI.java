package org.mets.mobile;

import java.util.ArrayList;

import org.mets.mobile.adapter.SectionListViewAdapter;
import org.mets.mobile.model.BPlusKPI;
import org.mets.mobile.model.EntryItem;
import org.mets.mobile.model.Item;
import org.mets.mobile.model.METSGlobalVariables;
import org.mets.mobile.model.SectionItem;
import org.mets.mobile.ui.listners.FacilityListItemClick;
import org.mets.mobile.ui.listners.ServerListener;
import org.mets.mobile.util.Helper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

/**
 * UI for Option B EID
 * 
 * @author jompango@gmail.com
 *
 */
public class EidUI extends Fragment implements ServerListener {

	private ListView eidListView;
	private TextView eidRefreshTextView;
	private TextView eidDataTitle;

	private FragmentManager eidParentFragmentManager;
	private Activity eidActivity;

	private METSGlobalVariables eidGlobalVariables;
	private BPlusKPI eidKPI;

	public EidUI(FragmentManager parentFragmentManager) {
		this.eidParentFragmentManager = parentFragmentManager;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		View rootView = inflater.inflate(R.layout.optionb_ui, container, false);

		this.eidActivity = this.getActivity();
		this.eidGlobalVariables = (METSGlobalVariables) eidActivity.getApplication();

		eidListView = (ListView) rootView.findViewById(R.id.listView);
		TextView parentPageTitle = (TextView) rootView.findViewById(R.id.parent_page_title_txt);
		TextView pageTitle = (TextView) rootView.findViewById(R.id.page_title_txt);
		eidDataTitle = (TextView) rootView.findViewById(R.id.title_txt);
		eidRefreshTextView = (TextView) rootView.findViewById(R.id.txt_last_updated);

		parentPageTitle.setText("Option B+");
		pageTitle.setText("EID");

		eidKPI = eidGlobalVariables.getmService().getBPlusKPI(eidActivity,
				eidGlobalVariables.getKpiEntity() + eidGlobalVariables.getKpiWeek());
		if (eidKPI != null)
			addItemsOnListView(eidKPI);
		else
			new populateUIAsync().execute();

		return rootView;
	}

	private void addItemsOnListView(BPlusKPI optionBKPI) {
		eidDataTitle.setText(eidGlobalVariables.getKpiEntity() + " " + eidGlobalVariables.getKpiWeek());
		eidRefreshTextView.setText(
				"Last updated: " + eidGlobalVariables.getmService().getLastRefreshed(eidActivity, Helper.KPI_KEY));

		ArrayList<Item> items = new ArrayList<Item>();
		items.add(new SectionItem("Number of EID Tests"));
		items.add(new EntryItem("Month", optionBKPI.getEidMonth()));
		items.add(new EntryItem("# of EID tests", optionBKPI.getNoEIDTests() + ""));
		items.add(new EntryItem("# Positive", optionBKPI.getNoPositive() + "%"));
		items.add(new EntryItem("Proportional Positive", optionBKPI.getPropPositive() + "%"));
		items.add(new EntryItem("Includes tests until", optionBKPI.getDateImported()));

		eidListView.setAdapter(new SectionListViewAdapter(eidActivity, items, 3));
		eidListView.setOnItemClickListener(new FacilityListItemClick(eidActivity, items, 3));
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.optionbplus, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@SuppressLint("NewApi")
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return Helper.MenuAction(item, eidActivity, eidParentFragmentManager);
	}

	private class populateUIAsync extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... params) {
			do {

			} while (!eidGlobalVariables.kpiFlag);

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			eidGlobalVariables.kpiFlag = false;
			eidKPI = eidGlobalVariables.getmService().getBPlusKPI(eidActivity,
					eidGlobalVariables.getKpiEntity() + eidGlobalVariables.getKpiWeek());
			addItemsOnListView(eidKPI);

		}

	}

	@Override
	public void newServerData(BPlusKPI kpi) {
		addItemsOnListView(kpi);
	}
}
