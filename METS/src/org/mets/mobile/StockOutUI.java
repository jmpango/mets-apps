package org.mets.mobile;

import java.util.ArrayList;

import org.mets.mobile.adapter.SectionListViewAdapter;
import org.mets.mobile.model.BPlusKPI;
import org.mets.mobile.model.EntryItem;
import org.mets.mobile.model.Item;
import org.mets.mobile.model.METSGlobalVariables;
import org.mets.mobile.model.SectionItem;
import org.mets.mobile.ui.listners.FacilityListItemClick;
import org.mets.mobile.ui.listners.ServerListener;
import org.mets.mobile.util.Helper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

/**
 * UI for Option B Stockout
 * 
 * @author jompango@gmail.com
 *
 */
public class StockOutUI extends Fragment implements ServerListener {

	private TextView sOutRefreshTextView;
	private TextView sOutDataTitle;
	private ListView stockoutListView;

	private FragmentManager sOutParentManager;
	private Activity sOutActivity;

	private METSGlobalVariables sOutGloblaVariable;
	private BPlusKPI sOutKPI;

	public StockOutUI(FragmentManager parentFragmentManager) {
		this.sOutParentManager = parentFragmentManager;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		View rootView = inflater.inflate(R.layout.optionb_ui, container, false);

		this.sOutActivity = this.getActivity();
		this.sOutGloblaVariable = (METSGlobalVariables) sOutActivity.getApplication();
		stockoutListView = (ListView) rootView.findViewById(R.id.listView);

		TextView parentPageTitle = (TextView) rootView.findViewById(R.id.parent_page_title_txt);
		TextView pageTitle = (TextView) rootView.findViewById(R.id.page_title_txt);
		sOutDataTitle = (TextView) rootView.findViewById(R.id.title_txt);
		sOutRefreshTextView = (TextView) rootView.findViewById(R.id.txt_last_updated);

		parentPageTitle.setText("Option B+");
		pageTitle.setText("Stock outs");

		sOutKPI = sOutGloblaVariable.getmService().getBPlusKPI(sOutActivity,
				sOutGloblaVariable.getKpiEntity() + sOutGloblaVariable.getKpiWeek());

		if (sOutKPI != null)
			addItemsOnListView(sOutKPI);
		else
			new populateUIAsync().execute();

		return rootView;
	}

	private void addItemsOnListView(BPlusKPI optionBKPI) {
		ArrayList<Item> items = new ArrayList<Item>();
		items.add(new SectionItem("Stockouts Reporting "));
		items.add(new EntryItem("View facilities without testkits", optionBKPI.getTestKitStockout() + ""));
		items.add(new EntryItem("View facilities without ARVs", optionBKPI.getArvStockout() + ""));

		sOutDataTitle.setText(sOutGloblaVariable.getKpiEntity() + " " + sOutGloblaVariable.getKpiWeek());
		sOutRefreshTextView.setText(
				"Last updated: " + sOutGloblaVariable.getmService().getLastRefreshed(sOutActivity, Helper.KPI_KEY));

		stockoutListView.setAdapter(new SectionListViewAdapter(sOutActivity, items, 0));
		stockoutListView.setOnItemClickListener(new FacilityListItemClick(sOutActivity, items, 0));
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.optionbplus, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@SuppressLint("NewApi")
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return Helper.MenuAction(item, sOutActivity, sOutParentManager);
	}

	private class populateUIAsync extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			do {

			} while (!sOutGloblaVariable.kpiFlag);

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			sOutGloblaVariable.kpiFlag = false;
			sOutKPI = sOutGloblaVariable.getmService().getBPlusKPI(sOutActivity,
					sOutGloblaVariable.getKpiEntity() + sOutGloblaVariable.getKpiWeek());
			addItemsOnListView(sOutKPI);

		}

	}

	@Override
	public void newServerData(BPlusKPI kpi) {
		addItemsOnListView(kpi);
	}
}
