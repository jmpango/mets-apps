package org.mets.mobile.service.ws;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.util.Log;

/**
 * This class is responsible to interacting with the webAPI
 * 
 * @author jompango@gmail.com
 *
 */
@SuppressLint("InlinedApi")
@SuppressWarnings("deprecation")
public class METSJsonParser {

	static InputStream is = null;
	static JSONObject jObj = null;
	static String json = "";
	static HttpClient httpClient;

	public METSJsonParser() {

	}

	@SuppressLint("NewApi")
	public String makeHttpRequest(String url, String method, List<NameValuePair> params) {
		try {

			if (method == "POST") {
				DefaultHttpClient httpClient = new DefaultHttpClient();
				// url = URLEncoder.encode(url, "utf-8");
				HttpPost httpPost = new HttpPost(url);
				httpPost.setEntity(new UrlEncodedFormEntity(params));

				HttpResponse httpResponse = httpClient.execute(httpPost);
				HttpEntity httpEntity = httpResponse.getEntity();
				is = httpEntity.getContent(); 

			} else if (method == "GET") {
				HttpClient httpClient = new DefaultHttpClient();
				String newURL = url.replace(" ", "%20");  
				HttpGet httpGet = new HttpGet(newURL);
				HttpResponse httpResponse = httpClient.execute(httpGet);

				if (httpResponse != null) {
					HttpEntity httpEntity = httpResponse.getEntity();
					is = httpEntity.getContent();
				} else {
					/*AppUtil.showLMessage("Remote server cannot be accessed. Contact help line", activity);*/
					System.out.println("I cant access the server.");
				}

			}

		} catch (ClientProtocolException e) {
			System.out.println("Client Protocol Exception.");
		} catch (IOException e) {
			System.out.println("IO Exception.");
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			json = sb.toString();

		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}

		if (json.startsWith("<head>")) { 
			/*AppUtil.showLMessage("Failed to connect to remote server wrong credentials", activity);*/
			return null;
		} else {

				//json = json.substring(json.indexOf("[") + 1, json.indexOf("]"));

		}
		return json;

	}
}
