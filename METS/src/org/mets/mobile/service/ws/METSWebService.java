package org.mets.mobile.service.ws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mets.mobile.model.BPlusReport;
import org.mets.mobile.model.Entity;
import org.mets.mobile.model.Facility;
import org.mets.mobile.model.FacilityContacts;
import org.mets.mobile.model.METSGlobalVariables;
import org.mets.mobile.model.Month;
import org.mets.mobile.model.BPlusKPI;
import org.mets.mobile.model.PedKPI;
import org.mets.mobile.model.RetKPI;
import org.mets.mobile.model.Week;
import org.mets.mobile.util.Helper;

import android.annotation.SuppressLint;
import android.util.Log;

/**
 * Bridge to connect the mobile application to the web service
 * 
 * @author jompango@gmail.com
 *
 */
@SuppressWarnings("deprecation")
@SuppressLint("NewApi")
public class METSWebService implements Serializable{

	private static final long serialVersionUID = 1L;

	private String initConnection(String url) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		METSJsonParser parser = new METSJsonParser();
		return parser.makeHttpRequest(url, "GET", params);

	}

	//http://ws.mets.or.ug/webAPI/optionb/getWeeks/10
	
	public boolean getWeeks(METSGlobalVariables globalVariable, int limit) {            

		String jsonString = initConnection(Helper.webServiceURL + "optionb/getWeeks/" + limit);
		try {
			if (jsonString != null && !jsonString.equalsIgnoreCase("")) {
				Log.d("METS App", "Retrieved week objects from remote server");
				String returnData = jsonString.toString();
				List<Week> weeks = new ArrayList<Week>();

				try {
					JSONArray jsonArray = new JSONArray(returnData);

					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject WeekData = jsonArray.getJSONObject(i);
						Week week = new Week();
						week.setWeekNo(WeekData.getString("weekNo"));
						week.setEndDate(WeekData.getString("endDate"));
						week.setStartDate(WeekData.getString("startDate"));
						week.setPeriodid(WeekData.getString("periodid"));
						weeks.add(week);
					}

					globalVariable.setDBItem(Helper.WEEKS_KEY, weeks);

				} catch (JSONException e) {
				}

				Log.d("METS App", "Weeks retrieved sucessfully from remote server");
				return true;
			}
		} catch (Exception e) {
			Log.e("METS Mobile ", e.getMessage());

		}
		return false;

	}

	public boolean getBPlusKPI(METSGlobalVariables globalVariables) {
		globalVariables.kpiFlag = false;
		String jsonString = initConnection(Helper.webServiceURL + "optionb/getKPI/" + globalVariables.getKpiEntity()
				+ "/" + globalVariables.getKpiWeek());
		try {
			if (jsonString != null && !jsonString.equalsIgnoreCase("")) {
				Log.d("METS App", "Retrieved kpi objects from remote server");
				String returnData = jsonString.toString();
				BPlusKPI kpi = new BPlusKPI();

				try {
					JSONObject kpiData = new JSONObject(returnData);

					kpi.setWeekno(kpiData.getString("weekno"));
					kpi.setWeeknoDisplay(kpiData.getString("weekno_display"));
					kpi.setLevel(kpiData.getString("level"));
					kpi.setEntity(kpiData.getString("entity"));
					kpi.setReportsRecieved(Integer.parseInt(kpiData.getString("reports_recieved")));
					kpi.setFacilitiesRegistered(Integer.parseInt(kpiData.getString("facilities_registered")));
					kpi.setReportingRate(
							(int) Math.round(Double.parseDouble(kpiData.getString("registered_reporting_rate"))));
					kpi.setAnc1Visits(Integer.parseInt(kpiData.getString("no_anc1_visits")));
					kpi.setTestedHIV(Integer.parseInt(kpiData.getString("no_tested_hiv")));
					kpi.setTestedPositive(Integer.parseInt(kpiData.getString("no_tested_pos")));
					kpi.setAtndKnownStatus(Integer.parseInt(kpiData.getString("no_atnd_known_status")));
					kpi.setInitiated(Integer.parseInt(kpiData.getString("no_initiated")));
					kpi.setAlreadyOnArt(Integer.parseInt(kpiData.getString("no_already_on_art")));
					kpi.setMissedAppointment(Integer.parseInt(kpiData.getString("no_missed_appointments")));
					kpi.setTestKitStockout(Integer.parseInt(kpiData.getString("test_kit_stockout")));
					kpi.setArvStockout(Integer.parseInt(kpiData.getString("arv_stockout")));
					kpi.setPctTotalTested(Integer.parseInt(kpiData.getString("pct_total_tested")));
					kpi.setPctTotalIntiated(Integer.parseInt(kpiData.getString("pct_total_initiated")));
					kpi.setEidMonth(kpiData.getString("eid_month"));
					kpi.setNoEIDTests(Integer.parseInt(kpiData.getString("no_children_tested")));
					kpi.setNoPositive(
							(int) Math.round(Double.parseDouble(kpiData.getString("no_children_tested_positive"))));
					kpi.setPropPositive((int) Math.round(Double.parseDouble(kpiData.getString("eid_date_reported"))));
					kpi.setDateImported(kpiData.getString("eid_data_imported"));

					globalVariables.setDBItem(globalVariables.getKpiEntity() + globalVariables.getKpiWeek(), kpi);
					globalVariables.kpiFlag = true;
				} catch (JSONException e) {
					Log.e("error", e.getMessage());
				}

				lastRefreshed(globalVariables, "kpi");

				Log.d("METS App", "KPI data retrieved sucessfully from remote server");
				return true;
			}
		} catch (Exception e) {
			Log.e("METS Mobile ", e.getMessage());

		}
		return false;
	}

	public boolean getBPlusFacilities(METSGlobalVariables globalVariable, String key) {
		String serverURL =  "optionb/getFacility/" + globalVariable.getKpiEntity() + "/"
				+ globalVariable.getKpiWeek();
		switch (key) {
		case "reported": 
			serverURL = serverURL + "/1";
			break;
		case "notreported":
			serverURL = serverURL + "/0";
			break;
		case "arvstockout":
			serverURL = serverURL + "/2";
			break;
		case "testkitstock":
			serverURL = serverURL + "/3";
			break;

		default:
			break;
		}

		String jsonString = initConnection(Helper.webServiceURL + serverURL);
		try {
			if (jsonString != null && !jsonString.equalsIgnoreCase("")) {
				Log.e("METS App", "Retrieved facility objects from remote server");
				String returnData = jsonString.toString();
				List<Facility> facilities = new ArrayList<Facility>();
				// returnData = "[" + returnData.replaceAll("\r",
				// "").replaceAll("\n", "") + "]";

				JSONArray jsonArray = new JSONArray(returnData);
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject facilityData = jsonArray.getJSONObject(i);
					Facility facility = new Facility();

					facility.setId(Integer.parseInt(facilityData.getString("organisationunitid")));
					facility.setName(facilityData.getString("health_facility"));
					facility.setSubCounty(facilityData.getString("subcounty"));
					facility.setDistrict(facilityData.getString("district"));
					facility.setOrgUnitGroup(facilityData.getString("org_unit_group"));
					facility.setSupportingIP(facilityData.getString("org_unit_group"));
					facility.setIsAcredited(facilityData.getString("art_accredited"));
					facility.setLevel(facilityData.getString("facility_level"));
					facility.setRegion(facilityData.getString("region"));
					facilities.add(facility);
				}

				globalVariable.setDBItem(key + globalVariable.getKpiEntity() + globalVariable.getKpiWeek(), facilities);

				lastRefreshed(globalVariable, "facility");

				Log.e("METS App", "facility data retrieved sucessfully from remote server");
				return true;
			}
		} catch (Exception e) {
			Log.e("METS Mobile ", e.getMessage());

		}
		return false;
	}

	public boolean getEntity(METSGlobalVariables globalVariable, String type, String selectedLevel) {
		String url = "optionb/getEntity/";
		if (type.equalsIgnoreCase("ret")) {
			url = "retention/getEntity/";
		} else if (type.equalsIgnoreCase("ped")) {
			url = "pediatric/getEntity/";
		}

		String jsonString = initConnection(Helper.webServiceURL + url + selectedLevel);
		try {
			if (jsonString != null && !jsonString.equalsIgnoreCase("")) {
				Log.d("METS App", "Retrieved " + type + " entity objects from remote server");
				String returnData = jsonString.toString();
				List<Entity> entities = new ArrayList<Entity>();

				try {
					JSONArray jsonArray = new JSONArray(returnData);

					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject entityData = jsonArray.getJSONObject(i);
						Entity entity = new Entity();
						entity.setName(entityData.getString("name"));

						entities.add(entity);
					}

					globalVariable.setDBItem(type + selectedLevel, entities);

				} catch (JSONException e) {
				}

				Log.d("METS App", type + " entities retrieved sucessfully from remote server");
				return true;
			}
		} catch (Exception e) {
			Log.e("METS Mobile ", e.getMessage());

		}
		return false;
	}

	public boolean getBPlusReports(METSGlobalVariables globalVariable, int facilityid, int limit) {
		String jsonString = initConnection(
				Helper.webServiceURL + "optionb/getReport/" + facilityid + "/" + limit);
		try {
			if (jsonString != null && !jsonString.equalsIgnoreCase("")) {
				Log.d("METS App", "Retrieved BPLus report for " + facilityid + " from remote server");
				String returnData = jsonString.toString();
				List<BPlusReport> bPlusReports = new ArrayList<BPlusReport>();

				try {
					JSONArray jsonArray = new JSONArray(returnData);

					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject reportData = jsonArray.getJSONObject(i);
						BPlusReport report = new BPlusReport();
						report.setOrganisationid(reportData.getString("organisationid"));
						report.setWeekno(reportData.getString("weekno"));
						report.setA(reportData.getString("a"));
						report.setB(reportData.getString("b"));
						report.setC(reportData.getString("c"));
						report.setD(reportData.getString("d"));
						report.setE(reportData.getString("e"));
						report.setF(reportData.getString("f"));
						report.setG(reportData.getString("g"));
						report.setH(reportData.getString("h"));
						report.setI(reportData.getString("i"));

						bPlusReports.add(report);
					}

					globalVariable.setDBItem("R:" + facilityid, bPlusReports);

				} catch (JSONException e) {
				}

				Log.d("METS App", "BPLus report for " + facilityid + " from remote server saved locally");
				return true;
			}
		} catch (Exception e) {
			Log.e("METS Mobile ", e.getMessage());

		}
		return false;
	}

	public boolean getUserContacts(METSGlobalVariables globalVariable, String facilityid) {
		String jsonString = initConnection(
				Helper.webServiceURL + "optionb/getContacts/" + facilityid);
		try {
			if (jsonString != null && !jsonString.equalsIgnoreCase("")) {
				Log.d("METS App", "Retrieved Facility Contacts for " + facilityid + " from remote server");
				String returnData = jsonString.toString();
				List<FacilityContacts> contacts = new ArrayList<FacilityContacts>();

				try {
					JSONArray jsonArray = new JSONArray(returnData);

					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject reportData = jsonArray.getJSONObject(i);
						FacilityContacts contact = new FacilityContacts();
						contact.setUsername(reportData.getString("username"));
						contact.setSurname(reportData.getString("surname"));
						contact.setFirstname(reportData.getString("firstname"));
						contact.setEmail(reportData.getString("email"));
						contact.setPhone_number(reportData.getString("phone_number"));
						contacts.add(contact);
					}

					globalVariable.setDBItem(facilityid, contacts);

				} catch (JSONException e) {
				}

				Log.d("METS App", "BPLus report for " + facilityid + " from remote server saved locally");
				return true;
			}
		} catch (Exception e) {
			Log.e("METS Mobile ", e.getMessage());

		}
		return false;
	}

	
	public boolean getMonts(METSGlobalVariables globalVariable, int limit, String type) {
		String url = "pediatric/getMonth/";
		if (type.equalsIgnoreCase("ret"))
			url = "retention/getMonth/";

		String jsonString = initConnection(Helper.webServiceURL + url + limit);
		try {
			if (jsonString != null && !jsonString.equalsIgnoreCase("")) {
				Log.d("METS App", "Retrieved month objects from remote server");
				String returnData = jsonString.toString();
				List<Month> months = new ArrayList<Month>();

				try {
					JSONArray jsonArray = new JSONArray(returnData);

					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject monthData = jsonArray.getJSONObject(i);
						Month month = new Month();
						month.setName(monthData.getString("name"));

						months.add(month);
					}

					globalVariable.setDBItem(type + "month", months);

				} catch (JSONException e) {
				}

				Log.d("METS App", "Months retrieved sucessfully from remote server");
				return true;
			}
		} catch (Exception e) {
			Log.e("METS Mobile ", e.getMessage());

		}
		return false;

	}

	public boolean getRetKPI(METSGlobalVariables globalVariables) {
		globalVariables.retFlag = false;
		String jsonString = initConnection(Helper.webServiceURL + "retention/getKPI/" + globalVariables.getRetEntity()
				+ "/" + globalVariables.getRetMonth());
		try {
			if (jsonString != null && !jsonString.equalsIgnoreCase("")) {
				Log.d("METS App", "Retrieved RET kpi objects from remote server");
				String returnData = jsonString.toString();
				List<RetKPI> rets = new ArrayList<RetKPI>();

				JSONArray jsonArray = new JSONArray(returnData);
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject kpiData = jsonArray.getJSONObject(i);

					RetKPI ret = new RetKPI();

					try {
						ret.setMonth(kpiData.getString("month"));
						ret.setName(kpiData.getString("name"));
						ret.setInitiated(Integer.parseInt(kpiData.getString("initiated")));
						ret.setMonth1(Integer.parseInt(kpiData.getString("month1")));
						ret.setMonth2(Integer.parseInt(kpiData.getString("month2")));
						ret.setMonth3(Integer.parseInt(kpiData.getString("month3")));
						ret.setName_month1(kpiData.getString("name_month1"));
						ret.setName_month2(kpiData.getString("name_month2"));
						ret.setName_month3(kpiData.getString("name_month3"));

					} catch (JSONException e) {
						Log.e("error", e.getMessage());
					}
					
					rets.add(ret);
				}

				globalVariables.setDBItem(globalVariables.getRetEntity() + globalVariables.getRetMonth(), rets);
				globalVariables.retFlag = true;
				lastRefreshed(globalVariables, "ret");

				Log.d("METS App", "RET data retrieved sucessfully from remote server");
				return true;
			}
		} catch (Exception e) {
			Log.e("METS Mobile ", e.getMessage());

		}
		return false;
	}

	public boolean getPedKPI(METSGlobalVariables globalVariables) {
		globalVariables.pedFlag = false;
		String jsonString = initConnection(Helper.webServiceURL + "pediatric/getKPI/" + globalVariables.getPedEntity()
				+ "/" + globalVariables.getPedMonth());
		try {
			if (jsonString != null && !jsonString.equalsIgnoreCase("")) {
				Log.d("METS App", "Retrieved PED kpi objects from remote server");
				String returnData = jsonString.toString();
				List<PedKPI> peds = new ArrayList<PedKPI>();

				JSONArray jsonArray = new JSONArray(returnData);
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject kpiData = jsonArray.getJSONObject(i);

					PedKPI ped = new PedKPI();

					try {
						ped.setMonth(kpiData.getString("month"));
						ped.setName(kpiData.getString("name"));
						ped.setOrganisationunitid(kpiData.getString("organisationunitid"));
						ped.setTotal_attendence(Integer.parseInt(kpiData.getString("total_attendence")));
						ped.setTotal_tested(Integer.parseInt(kpiData.getString("total_tested")));
						ped.setTotal_tested_positive(Integer.parseInt(kpiData.getString("total_tested_positive")));
						ped.setTotal_linked_care(Integer.parseInt(kpiData.getString("total_linked_care")));
						ped.setTotal_started_art(Integer.parseInt(kpiData.getString("total_started_art")));

					} catch (JSONException e) {
						Log.e("error", e.getMessage());
					}

					peds.add(ped);
				}

				globalVariables.setDBItem(globalVariables.getPedEntity() + globalVariables.getPedMonth(), peds);
				globalVariables.pedFlag = true;
				lastRefreshed(globalVariables, "ped");

				Log.d("METS App", "PED data retrieved sucessfully from remote server");
				return true;
			}
		} catch (Exception e) {
			Log.e("METS Mobile ", e.getMessage());

		}
		return false;
	}

	public void lastRefreshed(METSGlobalVariables globalVariables, String key) {
		String lastUpdated = "" + new Date();
		globalVariables.setDBItem(key, lastUpdated);
	}
}
