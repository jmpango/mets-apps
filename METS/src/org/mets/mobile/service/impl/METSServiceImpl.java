package org.mets.mobile.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.mets.mobile.model.BPlusKPI;
import org.mets.mobile.model.BPlusReport;
import org.mets.mobile.model.Entity;
import org.mets.mobile.model.Facility;
import org.mets.mobile.model.FacilityContacts;
import org.mets.mobile.model.METSGlobalVariables;
import org.mets.mobile.model.Month;
import org.mets.mobile.model.PedKPI;
import org.mets.mobile.model.RetKPI;
import org.mets.mobile.model.Week;
import org.mets.mobile.service.METSService;

import android.app.Activity;

/**
 * This is the service implementation for the METS mobile application
 * 
 * @author jompango@gmail.com
 *
 */
@SuppressWarnings("unchecked")
public class METSServiceImpl implements METSService, Serializable {

	private static final long serialVersionUID = 1L;

	public METSServiceImpl() {
	}

	@Override
	public String getLastRefreshed(Activity activity, String key) {
		return (String) ((METSGlobalVariables) activity.getApplication()).getDBItem(key);
	}

	@Override
	public List<String> getWeeks(Activity activity, String key) {
		List<Week> weeks = (List<Week>) ((METSGlobalVariables) activity.getApplication()).getDBItem(key);
		if (weeks != null) {
			List<String> weekList = new ArrayList<String>();

			for (Week w : weeks) {
				weekList.add(w.getWeekNo());
			}

			return weekList;
		}

		return null;
	}

	@Override
	public BPlusKPI getBPlusKPI(Activity activity, String key) {
		return (BPlusKPI) ((METSGlobalVariables) activity.getApplication()).getDBItem(key);
	}

	@Override
	public List<Facility> getFacilities(Activity activity, String key) {
		return (List<Facility>) ((METSGlobalVariables) activity.getApplication()).getDBItem(key);
	}

	@Override
	public List<String> getEntities(Activity activity, String key) {
		List<Entity> entities = (List<Entity>) ((METSGlobalVariables) activity.getApplication()).getDBItem(key);
		if (entities != null) {
			List<String> entityList = new ArrayList<String>();

			for (Entity e : entities) {
				entityList.add(e.getName());
			}

			return entityList;
		}

		return null;
	}

	@Override
	public List<BPlusReport> getBPlusReports(Activity activity, String key) {
		return (List<BPlusReport>) ((METSGlobalVariables) activity.getApplication()).getDBItem(key);
	}

	@Override
	public List<String> getMonths(Activity activity, String key) {
		List<Month> months = (List<Month>) ((METSGlobalVariables) activity.getApplication()).getDBItem(key);
		if (months != null) {
			List<String> monthList = new ArrayList<String>();

			for (Month e : months) {
				monthList.add(e.getName());
			}

			return monthList;
		}

		return null;
	}

	@Override
	public List<PedKPI> getPedKPI(Activity activity, String key) {
		return (List<PedKPI>) ((METSGlobalVariables) activity.getApplication()).getDBItem(key);
	}

	@Override
	public List<RetKPI> getRetKPI(Activity activity, String key) {
		return (List<RetKPI>) ((METSGlobalVariables) activity.getApplication()).getDBItem(key);
	}

	@Override
	public List<FacilityContacts> getUserContacts(Activity activity, String key) {
		return (List<FacilityContacts>) ((METSGlobalVariables) activity.getApplication()).getDBItem(key);
	}

}
