package org.mets.mobile.service;

import java.util.List;

import org.mets.mobile.model.BPlusKPI;
import org.mets.mobile.model.BPlusReport;
import org.mets.mobile.model.Facility;
import org.mets.mobile.model.FacilityContacts;
import org.mets.mobile.model.PedKPI;
import org.mets.mobile.model.RetKPI;

import android.app.Activity;

/**
 * Interface class for METS mobile app
 * 
 * @author jompango@gmail.com
 *
 */
public interface METSService {
	
	public List<String> getWeeks(Activity activity, String key);

	public BPlusKPI getBPlusKPI(Activity activity, String key);

	public List<Facility> getFacilities(Activity activity, String key);

	public List<String> getEntities(Activity activity, String key);

	public List<BPlusReport> getBPlusReports(Activity activity, String key);
	
	public List<FacilityContacts> getUserContacts(Activity activity, String key);

	public List<String> getMonths(Activity activity, String key);

	public List<PedKPI> getPedKPI(Activity activity, String key);

	public List<RetKPI> getRetKPI(Activity activity, String key);

	public String getLastRefreshed(Activity activity, String key);

}
