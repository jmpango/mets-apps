package org.mets.mobile;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.annotation.SuppressLint;

public class TestingUI {

	@SuppressLint("SimpleDateFormat")
	public static void main(String[] args) throws ParseException {
		String month = "";
		Calendar now = Calendar.getInstance();
		int num = (now.get(Calendar.MONTH)) - 1;
		int year = now.get(Calendar.YEAR);
		
		if(num < 0){
			num = 11;
			year = year - 1;
		}

		DateFormatSymbols dfs = new DateFormatSymbols();
		String[] months = dfs.getMonths();
		if (num >= 0 && num <= 11) {
			month = months[num];
		}
		
		System.out.println(month + " " + year);
		
	}
	
	static String getCurrentDate(){
		/*DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        Date myDate = new Date(System.currentTimeMillis());
        System.out.println("result is "+ dateFormat.format(myDate));
        Calendar cal = Calendar.getInstance();
        cal.setTime(myDate);
        cal.add(Calendar.DATE, -1);
        return dateFormat.format(cal.getTime());*/
		
		String name = "Agago District";
		if(name.contains("District"))
			name = name.substring(0, name.indexOf("District"));
		
		System.out.println(name.trim());
		return  null;
		/*new SimpleDateFormat("ddMMyyyy").format(new Date());
		 DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
	        Date myDate = dateFormat.parse(dateString);

	        Date oneDayBefore = new Date(myDate.getTime() - 2);

	       return dateFormat.format(oneDayBefore);*/
	}
	
	  static String getMonthForInt() {
	        String month = "";
	        Calendar now = Calendar.getInstance();
			int num = now.get(Calendar.MONTH);
			
	        DateFormatSymbols dfs = new DateFormatSymbols();
	        String[] months = dfs.getMonths();
	        if (num >= 0 && num <= 11 ) {
	            month = months[num];
	        }
	        return month + " " + now.get(Calendar.YEAR);
	    }
	
	public static int getLastWeekOfMonthInYear(int month, int year)
	{
	    Calendar cal = Calendar.getInstance();
	    cal.set(Calendar.YEAR, year);
	    cal.set(Calendar.MONTH, month);
	    cal.set(Calendar.DAY_OF_MONTH, 1);

	    return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
	}
	
	public static int getTotalWeeksInYear(int year) {
        Calendar mCalendar = new GregorianCalendar(); 
        mCalendar.set(Calendar.YEAR, year); // Set only year 
        mCalendar.set(Calendar.MONTH, 12); // Don't change
        mCalendar.set(Calendar.DAY_OF_MONTH, 31); // Don't change
        return mCalendar.get(Calendar.WEEK_OF_YEAR);
    }
}
