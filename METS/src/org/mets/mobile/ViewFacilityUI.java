package org.mets.mobile;

import java.util.ArrayList;
import java.util.List;

import org.mets.mobile.adapter.FacilityListViewAdapter;
import org.mets.mobile.model.Facility;
import org.mets.mobile.model.METSGlobalVariables;
import org.mets.mobile.util.Helper;
import org.mets.mobile.util.dialog.LoadingUI;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This is the startup for showing the List of Facilities
 * 
 * @author jmpango@gmail.com
 *
 */
public class ViewFacilityUI extends Activity implements OnItemSelectedListener, TextWatcher, OnItemClickListener {

	public static final String TAG = "View Facility";
	private String parentTitle, qFacility;
	private List<Facility> facilities;
	private ListView listView;
	private TextView filterTxtView;
	private METSGlobalVariables globalVariable;
	private LinearLayout contentLoadingLayout;

	public ViewFacilityUI() {
	}

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.view_facility);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		Intent intent = getIntent();

		this.parentTitle = intent.getStringExtra("parentTitle");
		this.qFacility = intent.getStringExtra("qFacilities");
		this.globalVariable = (METSGlobalVariables) this.getApplication();

		listView = (ListView) findViewById(R.id.listView);
		TextView parentPageTitle = (TextView) findViewById(R.id.parent_page_title_txt);
		TextView pageTitle = (TextView) findViewById(R.id.page_title_txt);
		TextView dataTitle = (TextView) findViewById(R.id.title_txt);
		filterTxtView = (TextView) findViewById(R.id.search_by_facility_name);
		filterTxtView.addTextChangedListener(this);
		contentLoadingLayout = (LinearLayout) findViewById(R.id.content_loading_layout);

		Spinner spinner = (Spinner) findViewById(R.id.search_by_facility_type);
		List<String> categories = new ArrayList<String>();
		categories.add("Filter by Level");
		categories.add("NR Hospital");
		categories.add("General Hospital");
		categories.add("Clinic");
		categories.add("HC IV");
		categories.add("HC III");
		categories.add("HC II");
		spinner.setOnItemSelectedListener(this);
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
				categories);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);

		parentPageTitle.setText(globalVariable.getKpiEntity());
		pageTitle.setText(globalVariable.getKpiWeek());
		dataTitle.setText(getParentTitle());

		displayUIItems();
	}

	private void displayUIItems() {
		String key = "registered";

		if (qFacility.equalsIgnoreCase("Registered Facilities")) {
			facilities = globalVariable.getmService()
					.getFacilities(this, "registered" + globalVariable.getKpiEntity() + globalVariable.getKpiWeek());
		} else if (qFacility.equalsIgnoreCase("Facilities Reported")) {
			facilities = globalVariable.getmService()
					.getFacilities(this, "reported" + globalVariable.getKpiEntity() + globalVariable.getKpiWeek());
			key = "reported";

		} else if (qFacility.equalsIgnoreCase("Facilities Not Reported")) {
			facilities = globalVariable.getmService()
					.getFacilities(this, "notreported" + globalVariable.getKpiEntity() + globalVariable.getKpiWeek());
			key = "notreported";
		} else if (qFacility.equalsIgnoreCase("Facilities Without Testkits")) {
			facilities = globalVariable.getmService()
					.getFacilities(this, "arvstockout" + globalVariable.getKpiEntity() + globalVariable.getKpiWeek());
			key = "arvstockout";
		} else if (qFacility.equalsIgnoreCase("Facilities Without ARVs")) {
			facilities = globalVariable.getmService()
					.getFacilities(this, "testkitstock" + globalVariable.getKpiEntity() + globalVariable.getKpiWeek());
			key = "testkitstock";
		}

		if (facilities == null) {
			new LoadFacilities(key).execute();
		} else {
			listView.setAdapter(new FacilityListViewAdapter(getApplicationContext(), facilities));
			listView.setOnItemClickListener(this);
		}
	}

	private class LoadFacilities extends AsyncTask<String, Void, String> {
		private LoadingUI loadingUI;
		private boolean isDataAvailable;
		private String key;

		public LoadFacilities(String key) {
			this.key = key;
		}

		@SuppressLint("NewApi")
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			contentLoadingLayout.setVisibility(View.VISIBLE);
			loadingUI = new LoadingUI();
			loadingUI.show(getFragmentManager(), "Loading facilities");
		}

		@Override
		protected String doInBackground(String... params) {
			isDataAvailable = globalVariable.getwService().getBPlusFacilities(globalVariable, key);
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			contentLoadingLayout.setVisibility(View.GONE);
			loadingUI.customDissmis();

			if (isDataAvailable) {
				displayUIItems();
			} else {
				// Helper.showLMessage("No data to display", getParent());
			}
		}
	}

	public String getParentTitle() {
		return parentTitle;
	}

	public void setParentTitle(String parentTitle) {
		this.parentTitle = parentTitle;
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		String selectedItem = parent.getItemAtPosition(position).toString();
		filterTxtView.setText("");
		if (!selectedItem.equals("Filter by Level")) {
			List<Facility> facilitiez = Helper.searchFacilityWithLevelType(selectedItem + "", facilities);
			if (facilitiez.size() > 0) {
				listView.setAdapter(new FacilityListViewAdapter(getApplicationContext(), facilitiez));
			} else {
				listView.setAdapter(new FacilityListViewAdapter(getApplicationContext(), facilitiez));
				Toast.makeText(this, "No match found with filter", Toast.LENGTH_LONG).show();
			}
		}

	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {

	}

	/*
	 * @Override public boolean onKeyDown(int keyCode, KeyEvent event) { if
	 * ((keyCode == KeyEvent.KEYCODE_BACK)) { finish(); } return
	 * super.onKeyDown(keyCode, event); }
	 * 
	 * @Override public void onBackPressed() { super.onBackPressed(); Intent
	 * homeIntent = new Intent(this, MainUI.class);
	 * homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	 * startActivity(homeIntent); //this.finish(); }
	 */

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		List<Facility> facilitiez = Helper.searchFacilityWithName(s + "", facilities);
		if (facilitiez.size() > 0) {
			listView.setAdapter(new FacilityListViewAdapter(getApplicationContext(), facilitiez));
		} else {
			if (!s.equals("")) {
				listView.setAdapter(new FacilityListViewAdapter(getApplicationContext(), facilitiez));
				Toast.makeText(this, "No match found with filter", Toast.LENGTH_LONG).show();
			}
		}
	}

	@Override
	public void afterTextChanged(Editable s) {

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Facility item = (Facility) facilities.get(position);
		Intent myIntent = new Intent(view.getContext(), ViewFacilityDetailsUI.class);
		myIntent.putExtra("parentTitle", parentTitle);
		myIntent.putExtra("facility", item);
		this.startActivity(myIntent);
	}

}
