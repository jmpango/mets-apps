package org.mets.mobile.fragment;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import org.mets.mobile.R;
import org.mets.mobile.model.FacilityLocation;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;

import android.app.Activity;
import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;
import jxl.read.biff.File;

/**
 * This is the startup for showing the find facility UI
 * 
 * @author jmpango@gmail.com
 *
 */
public class FacilityLocatorFragment extends Fragment {
	public static final String TAG = "Upload";
	private List<FacilityLocation> facilityLocations = new ArrayList<>();
	private static final String ARG_SECTION_NUMBER = "section_number";

	private GoogleMap googleMap;
	MapView mapView;
	GoogleMap map;

	public FacilityLocatorFragment() {
	}

	public static FacilityLocatorFragment newInstance(String uploadName) {
		FacilityLocatorFragment fragment = new FacilityLocatorFragment();
		Bundle args = new Bundle();
		args.putString(ARG_SECTION_NUMBER, uploadName);
		fragment.setArguments(args);
		return fragment;
	}

	/*
	 * private void initilizeMap() { if (googleMap == null) { googleMap =
	 * ((MapFragment) getFragmentManager().findFragmentById(
	 * R.id.map)).getMap();
	 * 
	 * // check if map is created successfully or not if (googleMap == null) {
	 * Toast.makeText(getApplicationContext(), "Sorry! unable to create maps",
	 * Toast.LENGTH_SHORT) .show(); } } }
	 */

	public View onCreateView(LayoutInflater inflater, ViewGroup container,

	Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.nav_find_facility_ui, container, false);

		if (facilityLocations.size() <= 0) {
			new generateExcelSheet().execute();
		}

		/*
		 * mMap = ((SupportMapFragment) getFragmentManager().findFragmentById(
		 * R.id.map)).getMap();
		 */
		
		// Getting status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity().getBaseContext());
 
        // Showing status
        if(status==ConnectionResult.SUCCESS)
        	try {
    			mapView = (MapView) rootView.findViewById(R.id.mapview);
    			mapView.onCreate(savedInstanceState);

    			// Gets to GoogleMap from the MapView and does initialization stuff
    			map = mapView.getMap();
    			/* map.getUiSettings().setMyLocationButtonEnabled(false); */
    /*			map.setMyLocationEnabled(false);*/
    			


    			MapsInitializer.initialize(this.getActivity());
    		} catch (Exception exception) {
    			Log.e("MAP EXEPTION", exception.toString());
    		}
        else{
        	TextView textView=(TextView) rootView.findViewById(R.id.find_facility);
        	textView.setText("Google Play Services not found, yet required for  Map on this page to Load");
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, getActivity(), requestCode);
            dialog.show();
        }
        
		
		return rootView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	private class generateExcelSheet extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			try {

				// Getting file stream of the Excel File containing facility
				// locations
				InputStream inputStream = getResources().openRawResource(R.raw.facility_list);

				// Initialising the workbook with the Inputstream that was
				// created
				Workbook wrk1 = Workbook.getWorkbook(inputStream);

				// Obtain the reference to the first sheet in the workbook
				Sheet sheet1 = wrk1.getSheet(0);

				for (int i = 1; i <= (sheet1.getColumn(0).length - 2000); i++) {
					// getting data from the each cell on a given row i and
					// creating an object FacilityLocation
					FacilityLocation facilityLocation = new FacilityLocation();
					facilityLocation.setName(sheet1.getCell(0, i).getContents());
					facilityLocation.setType(sheet1.getCell(1, i).getContents());
					facilityLocation.setLongititude(sheet1.getCell(2, i).getContents());
					facilityLocation.setLatitude(sheet1.getCell(3, i).getContents());
					facilityLocation.setRegion(sheet1.getCell(4, i).getContents());
					facilityLocation.setDistrict(sheet1.getCell(5, i).getContents());
					facilityLocation.setSubCounty(sheet1.getCell(6, i).getContents());

					// adding object FacilityLocation to the FacilityLocations
					// List
					facilityLocations.add(facilityLocation);
				}

				System.out.print(facilityLocations.size());
			} catch (BiffException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Toast toast = Toast.makeText(getActivity(), facilityLocations.size() + " Facility Locations Updated",
					Toast.LENGTH_LONG);
			toast.show();
		}

	}

}
