package org.mets.mobile.fragment;

import org.mets.mobile.R;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * This is the startup for showing the EID UI
 * @author jmpango@gmail.com
 *
 */
public class PedFragment extends Fragment {

	public static final String TAG = "ped";
	private static final String ARG_SECTION_NUMBER = "section_number";
	
	public PedFragment() {
	}

	public static PedFragment newInstance(String eidName) {
		PedFragment fragment = new PedFragment();
		Bundle args = new Bundle();
		args.putString(ARG_SECTION_NUMBER, eidName);
		fragment.setArguments(args);
		return fragment;
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.nav_ped_ui, container,
				false);

		return rootView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}
}
