package org.mets.mobile.fragment;

import org.mets.mobile.R;
import org.mets.mobile.adapter.OptionBPlusTabsPagerAdapter;

import android.annotation.SuppressLint;
import android.app.ActionBar.TabListener;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;

/**
 * This is the HomeUI for the METS mobile app
 * 
 * @author jmpangp@gmail.com
 *
 */
@SuppressLint("NewApi")
@SuppressWarnings("deprecation")
public class OptionBPlusFragment extends Fragment implements TabListener {

	public static final String TAG = OptionBPlusFragment.class.getSimpleName();

	private ViewPager viewPager;
	private OptionBPlusTabsPagerAdapter mAdapter;
	private String[] tabsTitles = { "R/Rates", "S/Outs", "ANC", "EID" };
	private FragmentManager parentFragmentManager;

	public static OptionBPlusFragment newInstance(FragmentManager parentFragmentManager) {
		return new OptionBPlusFragment(parentFragmentManager);
	}

	private OptionBPlusFragment(FragmentManager parentFragmentManager) {
		this.parentFragmentManager = parentFragmentManager;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.nav_optionb_plus_ui, container, false);
		final TabHost tabHost = (TabHost) view.findViewById(android.R.id.tabhost);
		tabHost.setup();

		for (int i = 0; i < tabsTitles.length; i++) {
			String tabName = tabsTitles[i];
			TabHost.TabSpec spec = tabHost.newTabSpec(tabName);
			spec.setContent(R.id.fakeTabContent);
			spec.setIndicator(tabName);
			tabHost.addTab(spec);
		}

		viewPager = (ViewPager) view.findViewById(R.id.pager);
		mAdapter = new OptionBPlusTabsPagerAdapter(getChildFragmentManager(), parentFragmentManager);
		viewPager.setAdapter(mAdapter);

		tabHost.setOnTabChangedListener(new OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				for (int i = 0; i < tabsTitles.length; i++) {
					if (tabId.equals(tabsTitles[i])) {
						viewPager.setCurrentItem(i);
						break;
					}
				}
			}
		});

		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				tabHost.setCurrentTab(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});

		return view;
	}

	@Override
	public void onTabSelected(android.app.ActionBar.Tab tab, android.app.FragmentTransaction ft) {

	}

	@Override
	public void onTabUnselected(android.app.ActionBar.Tab tab, android.app.FragmentTransaction ft) {

	}

	@Override
	public void onTabReselected(android.app.ActionBar.Tab tab, android.app.FragmentTransaction ft) {

	}
}
