package org.mets.mobile.fragment;

import java.util.ArrayList;
import java.util.List;

import org.mets.mobile.R;
import org.mets.mobile.async.OptionBPlusAsync;
import org.mets.mobile.model.BPlusFilter;
import org.mets.mobile.model.METSGlobalVariables;
import org.mets.mobile.model.RetKPI;
import org.mets.mobile.util.Helper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This is the startup for showing the Retention UI
 * 
 * @author jmpango@gmail.com
 *
 */
public class RetentionFragment extends Fragment {
	public static final String TAG = "Retention";
	private static final String ARG_SECTION_NUMBER = "section_number";
	private TableLayout tableLayout;
	private METSGlobalVariables globalVariables;
	private TextView rRateRefreshTextView;
	private TextView rTitleTxt;
	private TextView rMonthTxt;
	private List<RetKPI> retKPI;
	private Activity mActivity;
	private ProgressBar progressBar;
	private FragmentManager rParentFragmentManager;

	public static RetentionFragment newInstance(String retentionName, FragmentManager parentFragmentManager) {
		RetentionFragment fragment = new RetentionFragment(parentFragmentManager);
		Bundle args = new Bundle();
		args.putString(ARG_SECTION_NUMBER, retentionName);
		fragment.setArguments(args);
		return fragment;
	}

	public RetentionFragment(FragmentManager parentFragmentManager) {
		this.rParentFragmentManager = parentFragmentManager;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@SuppressLint("NewApi")
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.nav_retention_ui, container, false);

		this.mActivity = this.getActivity();
		
		mActivity.invalidateOptionsMenu();

		globalVariables = (METSGlobalVariables) mActivity.getApplication();
		tableLayout = (TableLayout) rootView.findViewById(R.id.tableView);
		rMonthTxt = (TextView) rootView.findViewById(R.id.month_txt);
		rTitleTxt = (TextView) rootView.findViewById(R.id.title_txt);
		rRateRefreshTextView = (TextView) rootView.findViewById(R.id.txt_last_updated);
		progressBar = (ProgressBar) rootView.findViewById(R.id.pbHeaderProgress);

		retKPI = globalVariables.getmService().getRetKPI(getActivity(),
				globalVariables.getRetEntity() + globalVariables.getRetMonth());

		if (retKPI != null) {
			addItemsOnUI(retKPI);
		} else {
			progressBar.setVisibility(View.VISIBLE);
			new populateUIAsync().execute();
		}

		return rootView;
	}

	@SuppressLint({ "InlinedApi", "NewApi" })
	private void addItemsOnUI(List<RetKPI> retKPI2) {
		progressBar.setVisibility(View.GONE);
		rRateRefreshTextView
				.setText("Last updated: " + globalVariables.getmService().getLastRefreshed(mActivity, Helper.RET_KEY));
		rTitleTxt.setText(Html.fromHtml("<b>Retention Report for " + globalVariables.getKpiEntity() + "</b>"));
		rMonthTxt.setText(Html.fromHtml(globalVariables.getRetMonth()));

		tableLayout.removeAllViews();

		/** ============= Header row =================== */
		TableRow headerRow = new TableRow(mActivity);
		TableLayout.LayoutParams headerRowParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT,
				TableLayout.LayoutParams.WRAP_CONTENT);
		headerRow.setWeightSum(1.0f);
		headerRowParams.setMargins(20, 0, 10, 20);
		headerRow.setLayoutParams(headerRowParams);
		headerRow.setBackgroundResource(R.drawable.bottom_line);

		TextView trDistrict = new TextView(mActivity);
		trDistrict.setText("District");
		trDistrict.setTypeface(Typeface.DEFAULT_BOLD);
		trDistrict.setGravity(Gravity.LEFT);
		trDistrict.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 0.35f));
		trDistrict.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 13);
		trDistrict.setTextColor(Color.parseColor("#949494"));
		trDistrict.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
		trDistrict.setPadding(0, 0, 0, 20);
		headerRow.addView(trDistrict);

		TextView trIntiated = new TextView(mActivity);
		trIntiated.setText(globalVariables.getRetMonth().subSequence(0, globalVariables.getRetMonth().length() - 4));
		trIntiated.setTypeface(Typeface.DEFAULT_BOLD);
		trIntiated.setGravity(Gravity.CENTER_HORIZONTAL);
		trIntiated.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 13);
		trIntiated.setTextColor(Color.parseColor("#949494"));
		trIntiated.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
		trIntiated.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 0.2f));
		trIntiated.setPadding(0, 0, 0, 20);
		headerRow.addView(trIntiated);

		TextView trFirstMonth = new TextView(mActivity);
		trFirstMonth.setText("1Mnth");
		trFirstMonth.setTypeface(Typeface.DEFAULT_BOLD);
		trFirstMonth.setGravity(Gravity.CENTER_HORIZONTAL);
		trFirstMonth.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 13);
		trFirstMonth.setTextColor(Color.parseColor("#949494"));
		trFirstMonth.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
		trFirstMonth.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 0.15f));
		trFirstMonth.setPadding(0, 0, 0, 20);
		headerRow.addView(trFirstMonth);

		TextView trSecondMonth = new TextView(mActivity);
		trSecondMonth.setText("2Mnth");
		trSecondMonth.setTypeface(Typeface.DEFAULT_BOLD);
		trSecondMonth.setGravity(Gravity.CENTER_HORIZONTAL);
		trSecondMonth.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 13);
		trSecondMonth.setTextColor(Color.parseColor("#949494"));
		trSecondMonth.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
		trSecondMonth.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 0.15f));
		trSecondMonth.setPadding(0, 0, 0, 20);
		headerRow.addView(trSecondMonth);

		TextView trThirdMonth = new TextView(mActivity);
		trThirdMonth.setText("3Mnth");
		trThirdMonth.setTypeface(Typeface.DEFAULT_BOLD);
		trThirdMonth.setGravity(Gravity.CENTER_HORIZONTAL);
		trThirdMonth.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 13);
		trThirdMonth.setTextColor(Color.parseColor("#949494"));
		trThirdMonth.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
		trThirdMonth.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 0.15f));
		trThirdMonth.setPadding(0, 0, 0, 20);
		headerRow.addView(trThirdMonth);
		tableLayout.addView(headerRow);

		if (retKPI2 != null) {
			for (RetKPI kpi : retKPI2) {
				TableRow contentRow = new TableRow(mActivity);
				contentRow.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
				contentRow.setWeightSum(1.0f);
				TableLayout.LayoutParams contentRowParams = new TableLayout.LayoutParams(
						TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT);
				contentRowParams.setMargins(20, 0, 10, 20);
				contentRow.setLayoutParams(headerRowParams);
				contentRow.setBackgroundResource(R.drawable.bottom_line);

				TextView trName = new TextView(mActivity);
				trName.setText(kpi.getName());
				trName.setGravity(Gravity.LEFT);
				trName.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 13);
				trName.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
				trName.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 0.35f));
				trName.setPadding(0, 20, 0, 40);
				contentRow.addView(trName);

				TextView trIntiatedValue = new TextView(mActivity);
				trIntiatedValue.setText(kpi.getInitiated() + "");
				trIntiatedValue.setGravity(Gravity.CENTER_HORIZONTAL);
				trIntiatedValue.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 13);
				trIntiatedValue.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
				trIntiatedValue.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 0.2f));
				trIntiatedValue.setPadding(0, 20, 0, 40);
				contentRow.addView(trIntiatedValue);

				TextView trFirstMonthValue = new TextView(mActivity);
				trFirstMonthValue.setText(kpi.getMonth1() + "");
				trFirstMonthValue.setGravity(Gravity.CENTER_HORIZONTAL);
				trFirstMonthValue.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 13);
				trFirstMonthValue.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
				trFirstMonthValue.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 0.15f));
				trFirstMonthValue.setPadding(0, 20, 0, 40);
				contentRow.addView(trFirstMonthValue);

				TextView trSecondMonthValue = new TextView(mActivity);
				trSecondMonthValue.setText(kpi.getMonth2() + "");
				trSecondMonthValue.setGravity(Gravity.CENTER_HORIZONTAL);
				trSecondMonthValue.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 13);
				trSecondMonthValue.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
				trSecondMonthValue.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 0.15f));
				trSecondMonthValue.setPadding(0, 20, 0, 40);
				contentRow.addView(trSecondMonthValue);

				TextView trThirdMonthValue = new TextView(mActivity);
				trThirdMonthValue.setText(kpi.getMonth3() + "");
				trThirdMonthValue.setGravity(Gravity.CENTER_HORIZONTAL);
				trThirdMonthValue.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 13);
				trThirdMonthValue.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
				trThirdMonthValue.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 0.15f));
				trThirdMonthValue.setPadding(0, 20, 0, 40);
				contentRow.addView(trThirdMonthValue);
				tableLayout.addView(contentRow);
			}

		}
	}

	private class populateUIAsync extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			do {

			} while (!globalVariables.retFlag);

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			globalVariables.kpiFlag = false;
			retKPI = globalVariables.getmService().getRetKPI(getActivity(),
					globalVariables.getRetEntity() + globalVariables.getRetMonth());
			addItemsOnUI(retKPI);
		}

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.optionbplus, menu);
		System.out.println("menu in retentionFragment");
		super.onCreateOptionsMenu(menu, inflater);
	}

	@SuppressLint("NewApi")
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_filter:
			System.out.println("menu in retentionFragment......");
			new FilterUI(mActivity, rParentFragmentManager).show(rParentFragmentManager, "retention filter Dialog");
			return true;
		/*
		 * case R.id.action_refresh: new LoadingUI().show(fragmentManager,
		 * "Loading Dialog"); return true;
		 */
		default:
			return false;
		}
	}

	@SuppressLint("NewApi")
	public class FilterUI extends DialogFragment {
		private static final String FILTER = "Filter";
		private static final String CANCEL = "Cancel";

		private Spinner spinnerMonth;
		private Spinner spinnerLevel;
		private Spinner spinnerIPorDistrict;

		private TextView loadingTextView;
		private ProgressBar loadingProgressBar;

		private List<String> levelCategories;

		private BPlusFilter iFilter;

		private METSGlobalVariables globalVariable;
		private FragmentManager parentFragmentManager;
		private Activity parentActivity;
		private Activity currentActivity;

		/*
		 * public OptionBFilterUI(Context parentContext, ) { }
		 */

		public FilterUI(Activity mActivity, FragmentManager parentFragmentManager) {
			this.parentActivity = mActivity;
			this.parentFragmentManager = parentFragmentManager;
		}

		@Override
		public void onDestroyView() {
			if (getDialog() != null && getRetainInstance())
				getDialog().setDismissMessage(null);
			super.onDestroyView();
		}

		@SuppressLint("InflateParams")
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setRetainInstance(true);

			this.currentActivity = this.getActivity();
			this.globalVariable = (METSGlobalVariables) currentActivity.getApplication();

			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			iFilter = new BPlusFilter();

			View customView = LayoutInflater.from(getActivity()).inflate(R.layout.retention_filter_ui, null, false);
			TextView titleTextView = (TextView) customView.findViewById(R.id.filter_month_label);
			loadingTextView = (TextView) customView.findViewById(R.id.loading);
			loadingProgressBar = (ProgressBar) customView.findViewById(R.id.filter_optionb_progress);

			spinnerMonth = (Spinner) customView.findViewById(R.id.filter_month);
			spinnerLevel = (Spinner) customView.findViewById(R.id.filter_level);
			spinnerIPorDistrict = (Spinner) customView.findViewById(R.id.filter_entity);

			spinnerMonth.setOnItemSelectedListener(new MonthSpinnerItemListner());
			spinnerLevel.setOnItemSelectedListener(new LevelSpinnerItemListner());
			spinnerIPorDistrict.setOnItemSelectedListener(new EntitySpinnerItemListner());

			hideProgressBar();

			populateSpinnerMonthDropDown(null);
			populateSpinnerLevelDropDown();

			new LoadMonthData().execute();

			titleTextView.setText(Html.fromHtml("<b><br/>Ministry of Health</b><br/>Retention Filter"));

			builder.setView(customView).setPositiveButton(CANCEL, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
					dialog.cancel();
				}
			}).setNegativeButton(FILTER, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {

					globalVariable.setRetEntity(iFilter.getEntity());
					globalVariable.setRetLevel(iFilter.getLevel());
					globalVariable.setRetMonth(iFilter.getWeekNo());

					dialog.dismiss();
					dialog.cancel();

					new OptionBPlusAsync(currentActivity, parentFragmentManager).execute();

				}
			});

			return builder.create();
		}

		private class MonthSpinnerItemListner implements OnItemSelectedListener {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				Spinner spinner = (Spinner) parent;
				iFilter.setWeekNo(spinner.getItemAtPosition(position).toString());

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}

		}

		private class LevelSpinnerItemListner implements OnItemSelectedListener {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				Spinner spinner = (Spinner) parent;
				String selectedItem = spinner.getItemAtPosition(position).toString();
				iFilter.setLevel(selectedItem);
				populateSpinnerIPorDistrictDropDown(selectedItem);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}

		}

		private class EntitySpinnerItemListner implements OnItemSelectedListener {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				Spinner spinner = (Spinner) parent;
				iFilter.setEntity(spinner.getItemAtPosition(position).toString());
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}

		}

		public void hideProgressBar() {
			loadingTextView.setVisibility(View.INVISIBLE);
			loadingProgressBar.setVisibility(View.INVISIBLE);
		}

		public void showProgressBar() {
			loadingTextView.setVisibility(View.VISIBLE);
			loadingProgressBar.setVisibility(View.VISIBLE);
		}

		private void populateSpinnerMonthDropDown(List<String> months) {
			if (months == null) {
				months = new ArrayList<String>();
				months.add(Helper.getCurrentReportingMonth());
			} else {
				spinnerMonth.setEnabled(true);
				spinnerLevel.setEnabled(true);
			}

			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity().getBaseContext(),
					android.R.layout.simple_spinner_item, months);

			dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerMonth.setAdapter(dataAdapter);
		}

		private void populateSpinnerLevelDropDown() {
			levelCategories = new ArrayList<String>();
			levelCategories.add("National");
			levelCategories.add("District");
			// levelCategories.add("Implementing Partner");

			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity().getBaseContext(),
					android.R.layout.simple_spinner_item, levelCategories);

			dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerLevel.setAdapter(dataAdapter);

		}

		private void populateSpinnerIPorDistrictDropDown(String selectedLevel) {
			List<String> entities = new ArrayList<String>();
			if (selectedLevel.equalsIgnoreCase("National")) {
				entities.add("Uganda");
				populateEntityDropdown(entities);
			} else if (selectedLevel.equals("District")) {
				entities = globalVariable.getmService().getEntities(currentActivity, "retDistrict");
				if (entities != null)
					populateEntityDropdown(entities);
				else {
					new LoadEntityData("District").execute();
				}

			} 
			/*else {
				entities = globalVariable.getmService().getEntities(currentActivity, "bplusIP");
				if (entities != null)
					populateEntityDropdown(entities);
				else {
					new LoadEntityData("IP").execute();
				}
			}*/

		}

		private void populateEntityDropdown(List<String> entities) {
			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity().getBaseContext(),
					android.R.layout.simple_spinner_item, entities);

			dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerIPorDistrict.setAdapter(dataAdapter);
			spinnerIPorDistrict.setEnabled(true);
		}

		@SuppressLint("ShowToast")
		public class LoadMonthData extends AsyncTask<String, Void, String> {
			private boolean isMonthDataAvailable = false;

			@Override
			protected void onPreExecute() {
				super.onPreExecute();

				spinnerMonth.setEnabled(false);
				spinnerIPorDistrict.setEnabled(false);
				spinnerLevel.setEnabled(false);

				showProgressBar();
			}

			@Override
			protected String doInBackground(String... params) {
				List<String> months = globalVariable.getmService().getMonths(currentActivity, "retmonth");

				if (months == null)
					isMonthDataAvailable = globalVariable.getwService().getMonts(globalVariable, 10, "ret");
				else {
					isMonthDataAvailable = true;
				}
				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				hideProgressBar();
				if (isMonthDataAvailable)
					populateSpinnerMonthDropDown(
							globalVariable.getmService().getMonths(currentActivity, "retmonth"));
				else
					Toast.makeText(parentActivity, "Failed to retrieve months, cancel and try again.",
							Toast.LENGTH_LONG);
			}

		}

		@SuppressLint("ShowToast")
		public class LoadEntityData extends AsyncTask<String, Void, String> {
			private boolean isEntityAvailable = false;
			private String level;

			public LoadEntityData(String level) {
				this.level = level;
			}

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				showProgressBar();
				spinnerIPorDistrict.setEnabled(false);
			}

			@Override
			protected String doInBackground(String... params) {
				isEntityAvailable = globalVariable.getwService().getEntity(globalVariable, "ret", level);
				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				hideProgressBar();
				spinnerIPorDistrict.setEnabled(true);
				if (isEntityAvailable)
					populateEntityDropdown(globalVariable.getmService().getEntities(currentActivity, "ret" + level));
				else
					Toast.makeText(parentActivity, "Failed to retrieve entities, cancel and try again.",
							Toast.LENGTH_LONG);
			}

		}
	}
}
