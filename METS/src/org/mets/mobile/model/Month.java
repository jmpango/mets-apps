package org.mets.mobile.model;

import java.io.Serializable;

public class Month implements Serializable {

	private static final long serialVersionUID = 1L;
	private String name;

	public Month() {
	}

	public Month(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
