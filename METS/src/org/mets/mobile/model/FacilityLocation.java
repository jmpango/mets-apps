package org.mets.mobile.model;

public class FacilityLocation {
	String name;
	String district;
	String region;
	String longititude;
	String latitude;
	String type;
	String subCounty;
	
	
	public FacilityLocation(String name, String district, String longititude, String latitude, String type,
			String subCounty,String region) {
		super();
		this.name = name;
		this.district = district;
		this.longititude = longititude;
		this.latitude = latitude;
		this.type = type;
		this.subCounty = subCounty;
		this.region=region;
	}
	
	
	public FacilityLocation() {
		// TODO Auto-generated constructor stub
	}


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getLongititude() {
		return longititude;
	}
	public void setLongititude(String longititude) {
		this.longititude = longititude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSubCounty() {
		return subCounty;
	}
	public void setSubCounty(String subCounty) {
		this.subCounty = subCounty;
	}


	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

}
