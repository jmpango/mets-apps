package org.mets.mobile.model;

import java.io.Serializable;
import java.util.HashMap;

import android.app.Application;

public class METSDatabase extends Application implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private HashMap<String, Object> database = new HashMap<String, Object>();

	public METSDatabase() {
	}

	public HashMap<String, Object> getDatabase() {
		return database;
	}

	public void setDatabase(HashMap<String, Object> database) {
		this.database = database;
	}

	public Object getDBItem(String key) {
		if (getDatabase().isEmpty()) {
			return null;
		}

		return getDatabase().get(key);
	}

	public void setDBItem(String key, Object value) {
		if (getDatabase().containsKey(key)) {
			getDatabase().remove(key);
		}
		getDatabase().put(key, value);
	}

}
