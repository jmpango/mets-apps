package org.mets.mobile.model;

/**
 * Used by SectionItem and EntryItem
 * @author jmpango@gmail.com
 *
 */
public interface Item {
	public boolean isSection();
}
