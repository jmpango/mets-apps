package org.mets.mobile.model;

/**
 * This class represents an item on the navigation drawer
 * 
 * @author jompango@gmail.com
 *
 */
public class NavigationDrawerItem {

	private int icon;
	private String title;
	private String count = "0";
	private boolean isCounterVisible = false;

	public NavigationDrawerItem() {
	}

	public NavigationDrawerItem(String title, int icon) {
		this.icon = icon;
		this.title = title;
	}

	public NavigationDrawerItem(String title, int icon,
			boolean isCounterVisible, String count) {
		this.title = title;
		this.icon = icon;
		this.isCounterVisible = isCounterVisible;
		this.count = count;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public boolean isCounterVisible() {
		return isCounterVisible;
	}

	public void setCounterVisible(boolean isCounterVisible) {
		this.isCounterVisible = isCounterVisible;
	}
	
	 public boolean getCounterVisibility(){
	        return this.isCounterVisible;
	    }
}
