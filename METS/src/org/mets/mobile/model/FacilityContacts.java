package org.mets.mobile.model;

import java.io.Serializable;

public class FacilityContacts implements Serializable {

	private static final long serialVersionUID = 1L;
	private String username;
	private String surname;
	private String firstname;
	private String email;
	private String phone_number;

	public FacilityContacts() {
	}

	public FacilityContacts(String username, String surname, String firstname, String email, String phone_number) {
		this.username = username;
		this.surname = surname;
		this.firstname = firstname;
		this.email = email;
		this.phone_number = phone_number;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}

}
