package org.mets.mobile.model;

import java.io.Serializable;

/**
 * Representation of the WebService Week entity
 * @author jompango@gmail.com
 *
 */
public class Week implements Serializable, Comparable<Facility> {
	private String weekNo;
	private String startDate;
	private String endDate;
	private String periodid;

	private static final long serialVersionUID = 1L;

	public Week() {
	}

	public Week(String periodid, String weekNo, String startDate, String endDate) {
		this.weekNo = weekNo;
		this.periodid = periodid;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public String getWeekNo() {
		return weekNo;
	}

	public void setWeekNo(String weekNo) {
		this.weekNo = weekNo;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getPeriodid() {
		return periodid;
	}

	public void setPeriodid(String periodid) {
		this.periodid = periodid;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@Override
	public int compareTo(Facility another) {
		return 0;
	}

}
