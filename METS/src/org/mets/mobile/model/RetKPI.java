package org.mets.mobile.model;

import java.io.Serializable;

public class RetKPI implements Serializable {

	private static final long serialVersionUID = 1L;
	private String month;
	private String name;
	private int initiated;
	private int month1;
	private int month2;
	private int month3;
	private String name_month1;
	private String name_month2;
	private String name_month3;

	public RetKPI() {
	}

	public RetKPI(String month, String name, int initiated, int month1, int month2, int month3, String name_month1,
			String name_month2, String name_month3) {
		this.month = month;
		this.name = name;
		this.initiated = initiated;
		this.month1 = month1;
		this.month2 = month2;
		this.month3 = month3;
		this.name_month1 = name_month1;
		this.name_month2 = name_month2;
		this.name_month3 = name_month3;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getInitiated() {
		return initiated;
	}

	public void setInitiated(int initiated) {
		this.initiated = initiated;
	}

	public int getMonth1() {
		return month1;
	}

	public void setMonth1(int month1) {
		this.month1 = month1;
	}

	public int getMonth2() {
		return month2;
	}

	public void setMonth2(int month2) {
		this.month2 = month2;
	}

	public int getMonth3() {
		return month3;
	}

	public void setMonth3(int month3) {
		this.month3 = month3;
	}

	public String getName_month1() {
		return name_month1;
	}

	public void setName_month1(String name_month1) {
		this.name_month1 = name_month1;
	}

	public String getName_month2() {
		return name_month2;
	}

	public void setName_month2(String name_month2) {
		this.name_month2 = name_month2;
	}

	public String getName_month3() {
		return name_month3;
	}

	public void setName_month3(String name_month3) {
		this.name_month3 = name_month3;
	}

}
