package org.mets.mobile.model;

import java.io.Serializable;

import org.mets.mobile.util.Helper;

public class BPlusKPI implements Serializable {
	private String weeknoDisplay;
	private String weekno = Helper.getCurrentReportingWeek();
	private String level = Helper.DEFAULT_BPLUS_LEVEL;
	private String entity = Helper.DEFAULT_BPLUS_LEVEL;
	private int reportsRecieved = 0;
	private int facilitiesRegistered = 0;
	private int reportingRate = 0;
	private int anc1Visits = 0;
	private int testedHIV = 0;
	private int testedPositive = 0;
	private int atndKnownStatus = 0;
	private int initiated = 0;
	private int alreadyOnArt = 0;
	private int missedAppointment = 0;
	private int testKitStockout = 0;
	private int arvStockout = 0;
	private int pctTotalTested = 0;
	private int pctTotalIntiated = 0;
	private String eidMonth = "";
	private int noEIDTests = 0;
	private int noPositive = 0;
	private int propPositive = 0;
	private String dateImported = "";

	private static final long serialVersionUID = 1L;

	public BPlusKPI() {
	}

	public BPlusKPI(String weekno, String weeknoDisplay, String level, String entity, int reportsRecieved,
			int facilitiesRegistered, int reportingRate, int anc1Visits, int testedHIV, int testedPositive,
			int atndKnownStatus, int initiated, int alreadyOnArt, int missedAppointment, int testKitStockout,
			int arvStockout, int pctTotalTested, int pctTotalIntiated, String eidMonth, int noEIDTests, int noPositive,
			int propPositive, String dateImported) {
		this.weekno = weekno;
		this.weeknoDisplay = weeknoDisplay;
		this.level = level;
		this.entity = entity;
		this.reportsRecieved = reportsRecieved;
		this.facilitiesRegistered = facilitiesRegistered;
		this.reportingRate = reportingRate;
		this.anc1Visits = anc1Visits;
		this.testedHIV = testedHIV;
		this.testedPositive = testedPositive;
		this.atndKnownStatus = atndKnownStatus;
		this.initiated = initiated;
		this.alreadyOnArt = alreadyOnArt;
		this.missedAppointment = missedAppointment;
		this.testKitStockout = testKitStockout;
		this.arvStockout = arvStockout;
		this.pctTotalTested = pctTotalTested;
		this.pctTotalIntiated = pctTotalIntiated;
		this.eidMonth = eidMonth;
		this.noEIDTests = noEIDTests;
		this.noPositive = noPositive;
		this.propPositive = propPositive;
		this.dateImported = dateImported;
	}

	public String getEidMonth() {
		return eidMonth;
	}

	public void setEidMonth(String eidMonth) {
		this.eidMonth = eidMonth;
	}

	public int getNoEIDTests() {
		return noEIDTests;
	}

	public void setNoEIDTests(int noEIDTests) {
		this.noEIDTests = noEIDTests;
	}

	public int getNoPositive() {
		return noPositive;
	}

	public void setNoPositive(int noPositive) {
		this.noPositive = noPositive;
	}

	public String getWeeknoDisplay() {
		return weeknoDisplay;
	}

	public void setWeeknoDisplay(String weeknoDisplay) {
		this.weeknoDisplay = weeknoDisplay;
	}

	public int getPropPositive() {
		return propPositive;
	}

	public void setPropPositive(int propPositive) {
		this.propPositive = propPositive;
	}

	public String getDateImported() {
		return dateImported;
	}

	public void setDateImported(String dateImported) {
		this.dateImported = dateImported;
	}

	public String getWeekno() {
		return weekno;
	}

	public void setWeekno(String weekno) {
		this.weekno = weekno;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public int getReportsRecieved() {
		return reportsRecieved;
	}

	public void setReportsRecieved(int reportsRecieved) {
		this.reportsRecieved = reportsRecieved;
	}

	public int getFacilitiesRegistered() {
		return facilitiesRegistered;
	}

	public void setFacilitiesRegistered(int facilitiesRegistered) {
		this.facilitiesRegistered = facilitiesRegistered;
	}

	public int getReportingRate() {
		return reportingRate;
	}

	public void setReportingRate(int reportingRate) {
		this.reportingRate = reportingRate;
	}

	public int getAnc1Visits() {
		return anc1Visits;
	}

	public void setAnc1Visits(int anc1Visits) {
		this.anc1Visits = anc1Visits;
	}

	public int getTestedHIV() {
		return testedHIV;
	}

	public void setTestedHIV(int testedHIV) {
		this.testedHIV = testedHIV;
	}

	public int getTestedPositive() {
		return testedPositive;
	}

	public void setTestedPositive(int testedPositive) {
		this.testedPositive = testedPositive;
	}

	public int getAtndKnownStatus() {
		return atndKnownStatus;
	}

	public void setAtndKnownStatus(int atndKnownStatus) {
		this.atndKnownStatus = atndKnownStatus;
	}

	public int getInitiated() {
		return initiated;
	}

	public void setInitiated(int initiated) {
		this.initiated = initiated;
	}

	public int getAlreadyOnArt() {
		return alreadyOnArt;
	}

	public void setAlreadyOnArt(int alreadyOnArt) {
		this.alreadyOnArt = alreadyOnArt;
	}

	public int getMissedAppointment() {
		return missedAppointment;
	}

	public void setMissedAppointment(int missedAppointment) {
		this.missedAppointment = missedAppointment;
	}

	public int getTestKitStockout() {
		return testKitStockout;
	}

	public void setTestKitStockout(int testKitStockout) {
		this.testKitStockout = testKitStockout;
	}

	public int getArvStockout() {
		return arvStockout;
	}

	public void setArvStockout(int arvStockout) {
		this.arvStockout = arvStockout;
	}

	public int getPctTotalTested() {
		return pctTotalTested;
	}

	public void setPctTotalTested(int pctTotalTested) {
		this.pctTotalTested = pctTotalTested;
	}

	public int getPctTotalIntiated() {
		return pctTotalIntiated;
	}

	public void setPctTotalIntiated(int pctTotalIntiated) {
		this.pctTotalIntiated = pctTotalIntiated;
	}

}
