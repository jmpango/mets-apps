package org.mets.mobile.model;

import java.io.Serializable;

public class PedKPI implements Serializable {

	private static final long serialVersionUID = 1L;
	private String organisationunitid;
	private String month;
	private String name;
	private int total_attendence;
	private int total_tested;
	private int total_tested_positive;
	private int total_linked_care;
	private int total_started_art;

	public PedKPI() {
	}

	public PedKPI(String organisationunitid, String month, String name, int total_attendence, int total_tested,
			int total_tested_positive, int total_linked_care, int total_started_art) {
		this.month = month;
		this.name = name;
		this.total_attendence = total_attendence;
		this.total_tested = total_tested;
		this.total_tested_positive = total_tested_positive;
		this.total_linked_care = total_linked_care;
		this.total_started_art = total_started_art;
		this.organisationunitid = organisationunitid;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTotal_attendence() {
		return total_attendence;
	}

	public void setTotal_attendence(int total_attendence) {
		this.total_attendence = total_attendence;
	}

	public int getTotal_tested() {
		return total_tested;
	}

	public void setTotal_tested(int total_tested) {
		this.total_tested = total_tested;
	}

	public int getTotal_tested_positive() {
		return total_tested_positive;
	}

	public void setTotal_tested_positive(int total_tested_positive) {
		this.total_tested_positive = total_tested_positive;
	}

	public int getTotal_linked_care() {
		return total_linked_care;
	}

	public void setTotal_linked_care(int total_linked_care) {
		this.total_linked_care = total_linked_care;
	}

	public int getTotal_started_art() {
		return total_started_art;
	}

	public void setTotal_started_art(int total_started_art) {
		this.total_started_art = total_started_art;
	}

	public String getOrganisationunitid() {
		return organisationunitid;
	}

	public void setOrganisationunitid(String organisationunitid) {
		this.organisationunitid = organisationunitid;
	}

}
