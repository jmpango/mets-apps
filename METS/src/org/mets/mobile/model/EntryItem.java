package org.mets.mobile.model;

/**
 * Represents a row content on the listview
 * 
 * @author jmpango@gmail.comm
 *
 */
public class EntryItem implements Item {

	private String lableName;
	private String value;

	public EntryItem(String lableName, String value) {
		this.lableName = lableName;
		this.value = value;
	}

	public String getLableName() {
		return lableName;
	}

	public void setLableName(String lableName) {
		this.lableName = lableName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public boolean isSection() {
		return false;
	}

}
