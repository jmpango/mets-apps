package org.mets.mobile.model;

public class RetFilter {
	private String monthNo;
	private String level;
	private String entity;

	public RetFilter() {
	}

	public RetFilter(String monthNo, String level, String entity) {
		this.monthNo = monthNo;
		this.level = level;
		this.entity = entity;
	}

	public String getMonthNo() {
		return monthNo;
	}

	public void setMonthNo(String monthNo) {
		this.monthNo = monthNo;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

}

