package org.mets.mobile.model;

import java.io.Serializable;
import java.util.List;

/**
 * Represents a facility
 * 
 * @author jompango@gmail.com
 *
 */
public class Facility implements Serializable, Comparable<Facility> {
	private static final long serialVersionUID = 1L;

	private int id;
	private String name;
	private String district;
	private String subCounty;
	private String supportingIP;
	private String level;
	private String region;
	private String isAcredited;
	private String orgUnitGroup;
	private List<FacilityContacts> contacts;

	public Facility() {
		super();
	}

	public Facility(int id, String name, String district, String subCounty, String supportingIP, String level,
			String region, String orgUnitGroup, String isAcredited) {
		this.id = id;
		this.name = name;
		this.district = district;
		this.subCounty = subCounty;
		this.supportingIP = supportingIP;
		this.level = level;
		this.region = region;
		this.orgUnitGroup = orgUnitGroup;
		this.isAcredited = isAcredited;

	}

	public Facility(int id, String name, String district, String subCounty, String supportingIP, String level,
			String region, String orgUnitGroup, String isAcredited, List<FacilityContacts> contacts) {
		this.id = id;
		this.name = name;
		this.district = district;
		this.subCounty = subCounty;
		this.supportingIP = supportingIP;
		this.level = level;
		this.region = region;
		this.orgUnitGroup = orgUnitGroup;
		this.isAcredited = isAcredited;
		this.contacts = contacts;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getSubCounty() {
		return subCounty;
	}

	public void setSubCounty(String subCounty) {
		this.subCounty = subCounty;
	}

	public String getSupportingIP() {
		return supportingIP;
	}

	public void setSupportingIP(String supportingIP) {
		this.supportingIP = supportingIP;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getIsAcredited() {
		return isAcredited;
	}

	public void setIsAcredited(String isAcredited) {
		this.isAcredited = isAcredited;
	}

	public String getOrgUnitGroup() {
		return orgUnitGroup;
	}

	public void setOrgUnitGroup(String orgUnitGroup) {
		this.orgUnitGroup = orgUnitGroup;
	}

	public List<FacilityContacts> getContacts() {
		return contacts;
	}

	public void setContacts(List<FacilityContacts> contacts) {
		this.contacts = contacts;
	}

	@Override
	public int compareTo(Facility facility) {
		if (this.getName() == null || facility.getName() == null)
			return 0;
		return this.name.compareToIgnoreCase(facility.name);
	}
}
