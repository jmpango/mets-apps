package org.mets.mobile.model;

import java.io.Serializable;
import java.util.HashMap;

import org.mets.mobile.service.METSService;
import org.mets.mobile.service.ws.METSWebService;
import org.mets.mobile.ui.listners.ServerListener;
import org.mets.mobile.util.Helper;
import org.mets.mobile.util.dialog.LoadingUI;

import android.app.Application;

/**
 * responsible for keep global variables used within the entire application
 * 
 * @author jompango@gmail.com
 *
 */

public class METSGlobalVariables extends Application implements Serializable {

	private static final long serialVersionUID = 1L;

	private String kpiWeek = Helper.getCurrentReportingWeek();
	private String kpiEntity = Helper.DEFAULT_BPLUS_ENTITY;
	private String kpiLevel = Helper.DEFAULT_BPLUS_LEVEL;

	private String retMonth = Helper.getCurrentReportingMonth();
	private String retEntity = Helper.DEFAULT_RET_ENTITY;
	private String retLevel = Helper.DEFAULT_RET_LEVEL;

	private String pedMonth = Helper.getCurrentReportingMonth();
	private String pedEntity = Helper.DEFAULT_PED_ENTITY;
	private String pedLevel = Helper.DEFAULT_PED_LEVEL;

	private LoadingUI loadingUI;
	private METSService mService;
	private METSWebService wService;
	private ServerListener apiListner;
	
	/* private FragmentManager fragmentManager; */
	private HashMap<String, Object> database = new HashMap<String, Object>();

	public boolean kpiFlag = false;
	public boolean retFlag = false;
	public boolean pedFlag = false;

	public METSGlobalVariables() {
	}

	public String getKpiWeek() {
		return kpiWeek;
	}

	public void setKpiWeek(String kpiWeek) {
		this.kpiWeek = kpiWeek;
	}

	public String getKpiEntity() {
		return kpiEntity;
	}

	public void setKpiEntity(String kpiEntity) {
		this.kpiEntity = kpiEntity;
	}

	public String getKpiLevel() {
		return kpiLevel;
	}

	public void setKpiLevel(String kpiLevel) {
		this.kpiLevel = kpiLevel;
	}

	public LoadingUI getLoadingUI() {
		return loadingUI;
	}

	public void setLoadingUI(LoadingUI loadingUI) {
		this.loadingUI = loadingUI;
	}

	public METSService getmService() {
		return mService;
	}

	public void setmService(METSService mService) {
		this.mService = mService;
	}

	public String getRetMonth() {
		return retMonth;
	}

	public void setRetMonth(String retMonth) {
		this.retMonth = retMonth;
	}

	public String getRetEntity() {
		return retEntity;
	}

	public void setRetEntity(String retEntity) {
		this.retEntity = retEntity;
	}

	public String getRetLevel() {
		return retLevel;
	}

	public void setRetLevel(String retLevel) {
		this.retLevel = retLevel;
	}

	public String getPedMonth() {
		return pedMonth;
	}

	public void setPedMonth(String pedMonth) {
		this.pedMonth = pedMonth;
	}

	public String getPedEntity() {
		return pedEntity;
	}

	public void setPedEntity(String pedEntity) {
		this.pedEntity = pedEntity;
	}

	public String getPedLevel() {
		return pedLevel;
	}

	public void setPedLevel(String pedLevel) {
		this.pedLevel = pedLevel;
	}

	public METSWebService getwService() {
		return wService;
	}

	public void setwService(METSWebService wService) {
		this.wService = wService;
	}

	public HashMap<String, Object> getDatabase() {
		return database;
	}

	public void setDatabase(HashMap<String, Object> database) {
		this.database = database;
	}

	public Object getDBItem(String key) {
		if (getDatabase().isEmpty()) {
			return null;
		}

		return getDatabase().get(key);
	}

	public void setDBItem(String key, Object value) {
		if (getDatabase().containsKey(key)) {
			getDatabase().remove(key);
		}
		getDatabase().put(key, value);
	}

	public ServerListener getApiListner() {
		return apiListner;
	}

	public void setApiListner(ServerListener apiListner) {
		this.apiListner = apiListner;
	}
}
