package org.mets.mobile.model;

public class BPlusFilter {
	private String weekNo;
	private String level;
	private String entity;

	public BPlusFilter() {
	}

	public BPlusFilter(String weekNo, String level, String entity) {
		this.weekNo = weekNo;
		this.level = level;
		this.entity = entity;
	}

	public String getWeekNo() {
		return weekNo;
	}

	public void setWeekNo(String weekNo) {
		this.weekNo = weekNo;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

}
