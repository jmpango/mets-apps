package org.mets.mobile.model;

import java.io.Serializable;
import java.util.Date;

/**
 * This class represents GIS coordinates captured for a location
 * 
 * @author jmpango
 * 
 */
public class GISCoordinates implements Comparable<GISCoordinates>, Serializable {
	private static final long serialVersionUID = 1L;

	private long id;
	private String serverId;
	private String name;
	private double longitude;
	private double latitude;
	private Date uploadDate;
	private int status = 0;
	private boolean selected = true;
	private String simSerialNo;

	public GISCoordinates() {

	}

	public GISCoordinates(String serverId, String name, double latitude,
			double longitude, String simSerialNo, Date uploadDate) {
		this.serverId = serverId;
		this.name = name;
		this.longitude = longitude;
		this.latitude = latitude;
		this.simSerialNo = simSerialNo;
		this.uploadDate = uploadDate;
	}

	public GISCoordinates(String url, String name, double longitude,
			double latitude, String simSerialNo, Date uploadDate, int status) {
		this.serverId = url;
		this.name = name;
		this.longitude = longitude;
		this.latitude = latitude;
		this.status = status;
		this.simSerialNo = simSerialNo;
		this.uploadDate = uploadDate;
	}

	public GISCoordinates(long id, String serverId, String name, double longitude,
			double latitude, String simSerialNo, Date uploadDate, int status) {
		this.id = id;
		this.serverId = serverId;
		this.name = name;
		this.longitude = longitude;
		this.latitude = latitude;
		this.status = status;
		this.simSerialNo = simSerialNo;
		this.uploadDate = uploadDate;
	}

	public GISCoordinates(long id, String serverId, String name, double longitude,
			double latitude, String simSerialNo, Date uploadDate, int status, boolean isSelected) {
		this.id = id;
		this.serverId = serverId;
		this.name = name;
		this.longitude = longitude;
		this.latitude = latitude;
		this.status = status;
		this.simSerialNo = simSerialNo;
		this.uploadDate = uploadDate;
		this.selected = isSelected;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getSimSerialNo() {
		return simSerialNo;
	}

	public void setSimSerialNo(String simSerialNo) {
		this.simSerialNo = simSerialNo;
	}

	@Override
	public int compareTo(GISCoordinates gisCoordinates) {
		return getUploadDate().compareTo(gisCoordinates.getUploadDate());
		/*if (this.getName() == null || gisCoordinates.getName() == null)
			return 0;
		return this.name.compareToIgnoreCase(gisCoordinates.name);*/
	}

}
