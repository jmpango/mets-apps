package org.mets.mobile;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Responsible for creating sharedPrefences for the METS app
 * @author Mpango
 *
 */
public class UserSettingUI extends PreferenceActivity {

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.settings);

	}
}
